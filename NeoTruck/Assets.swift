//
//  Assets.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 9/15/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class Assets {
    
    public class func wishlist(size:CGSize, color: UIColor, lineWidth:CGFloat) -> UIImage {
        var red:CGFloat = 0, green:CGFloat = 0, blue:CGFloat = 0, alpha:CGFloat = 0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        
        context.setStrokeColor(red: red, green: green, blue: blue, alpha: alpha)
        context.setFillColor(red: red, green: green, blue: blue, alpha: alpha)
        let pathRef = CGMutablePath()
        
        pathRef.move(to: CGPoint(x: lineWidth/2, y: lineWidth/2))
        pathRef.addLine(to: CGPoint(x: lineWidth/2, y: size.height-lineWidth/2))
        pathRef.addLine(to: CGPoint(x: size.width/2, y: 0.7 * size.height))
        pathRef.addLine(to: CGPoint(x: size.width-lineWidth/2, y: size.height-lineWidth/2))
        pathRef.addLine(to: CGPoint(x: size.width-lineWidth/2, y: lineWidth/2))
        pathRef.addLine(to: CGPoint(x: lineWidth/2, y: lineWidth/2))
        
        context.addPath(pathRef)
        context.setLineWidth(lineWidth)
        
        if lineWidth == 0 {
            context.fillPath()
        } else {
            context.strokePath()
        }
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img!
    }
    
    public class func back(size:CGSize, color: UIColor, lineWidth:CGFloat) -> UIImage {
        var red:CGFloat = 0, green:CGFloat = 0, blue:CGFloat = 0, alpha:CGFloat = 0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        
        context.setStrokeColor(red: red, green: green, blue: blue, alpha: alpha)
        let pathRef = CGMutablePath()
        
        pathRef.move(to: CGPoint(x: lineWidth/2, y: lineWidth/2))
        pathRef.addLine(to: CGPoint(x: size.width/2, y: size.height/2))
        pathRef.addLine(to: CGPoint(x: lineWidth/2, y: size.height-lineWidth/2))
        
        context.addPath(pathRef)
        context.setLineWidth(lineWidth)
        context.strokePath()
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img!
    }
    
    public class func leftarrow(size:CGSize, color: UIColor, lineWidth:CGFloat) -> UIImage {
        var red:CGFloat = 0, green:CGFloat = 0, blue:CGFloat = 0, alpha:CGFloat = 0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        
        context.setStrokeColor(red: red, green: green, blue: blue, alpha: alpha)
        let pathRef = CGMutablePath()
        
        pathRef.move(to: CGPoint(x: size.width-lineWidth/2, y: lineWidth/2))
        pathRef.addLine(to: CGPoint(x: size.width/2, y: size.height/2))
        pathRef.addLine(to: CGPoint(x: size.width-lineWidth/2, y: size.height-lineWidth/2))
        
        context.addPath(pathRef)
        context.setLineWidth(lineWidth)
        context.strokePath()
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img!
    }
    
    public class func rightarrow(size:CGSize, color: UIColor, lineWidth:CGFloat) -> UIImage {
        var red:CGFloat = 0, green:CGFloat = 0, blue:CGFloat = 0, alpha:CGFloat = 0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        
        context.setStrokeColor(red: red, green: green, blue: blue, alpha: alpha)
        let pathRef = CGMutablePath()
        
        pathRef.move(to: CGPoint(x: lineWidth/2, y: lineWidth/2))
        pathRef.addLine(to: CGPoint(x: size.width/2, y: size.height/2))
        pathRef.addLine(to: CGPoint(x: lineWidth/2, y: size.height-lineWidth/2))
        
        context.addPath(pathRef)
        context.setLineWidth(lineWidth)
        context.strokePath()
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img!
    }
    
}
