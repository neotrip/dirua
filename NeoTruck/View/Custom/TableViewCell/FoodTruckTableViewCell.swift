//
//  FoodTruckCell.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 30/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class FoodTruckTableViewCell: UITableViewCell {

    @IBOutlet weak var truckImage: ImageView!
    @IBOutlet weak var truckOpen: View!
    @IBOutlet weak var truckCover: VisualEffectGradientImageView!
    @IBOutlet weak var truckCategory: UIImageView!
    @IBOutlet weak var truckWishlist: UIImageView!
    @IBOutlet weak var truckName: UILabel!
    @IBOutlet weak var truckRating: CosmosView!
    
    
    var sessionCover: URLSessionDataTask?
    var sessionAvatar: URLSessionDataTask?
    
    func fill(_ truck: Truck){
        if let name = truck.name{
            truckName.text = name
        }
        
        if let rate = truck.rate {
            truckRating.rating = rate
            truckRating.isHidden = false
            truckRating.updateOnTouch = false
        } else {
            truckRating.isHidden = true
        }
        
        self.truckCategory.image = nil
        if let category = truck.categories?.first {
            if (category.name != "TODOS"){
                (_, _) = ServerManager.sharedInstance.image(category.image) { (error, image) in
                    if error == nil{
                        DispatchQueue.main.async(execute: {
                            self.truckCategory.image = image
                            self.truckCategory.addShadow()
                        })
                    }
                }
            }
        }
        
        truckOpen.backgroundColor = UIColor.appRed()
        if (truck.isOpen()){
            truckOpen.backgroundColor = UIColor.appGreen()
        }
        
        (_, sessionCover) = ServerManager.sharedInstance.image(truck.cover) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    truck.coverImage = image
                    self.truckCover.image = image
                })
            }
        }
        
        (_, sessionAvatar) = ServerManager.sharedInstance.image(truck.avatar) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    truck.logoImage = image
                    self.truckImage.image = image
                })
            }
        }
    }
    
}
