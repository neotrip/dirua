//
//  UserRatingTableViewCell.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 13/10/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import FBSDKLoginKit

@IBDesignable public class UserRatingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pictureImageView: FBSDKProfilePictureView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var ratingDateLabel: UILabel!
    
    func fill(_ rating: Rating){
        descriptionLabel.text = rating.comment
        rateView.rating = rating.general.doubleValue
        pictureImageView.profileID = rating.facebook!
        ratingDateLabel.text = rating.createdAt?.show()
        
        FacebookManager.sharedInstance.name(id: rating.facebook!) { (name) in
            if name != nil {
                DispatchQueue.main.async(execute: {
                    self.titleLabel.text = name
                })
            } else {
                DispatchQueue.main.async(execute: {
                    self.titleLabel.text = "(anônimo)"
                })
            }
        }
    }
}
