//
//  RatingCell.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 15/09/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class RatingTableViewCell: UITableViewCell {

    @IBOutlet weak var truckImage: UIImageView!
    @IBOutlet weak var truckCover: VisualEffectGradientImageView!
    @IBOutlet weak var truckName: UILabel!
    @IBOutlet weak var truckCategory: UIImageView!
    @IBOutlet weak var truckOpen: View!
    @IBOutlet weak var truckRating: CosmosView!
    @IBOutlet weak var geralTruckRating: CosmosView!
    
    var sessionCover: URLSessionDataTask?
    var sessionAvatar: URLSessionDataTask?
    
    func fill(_ rating: Rating){
        truckName.text = rating.truck.name
        truckRating.settings.totalStars = rating.general.intValue
        
        if let rate = rating.truck.rate {
            geralTruckRating.rating = Double(rate)
            geralTruckRating.isHidden = false
            geralTruckRating.updateOnTouch = false
        } else {
            geralTruckRating.isHidden = true
        }
        
        if let category = rating.truck.categories?.first {
            if (category.name != "TODOS"){
                (_, _) = ServerManager.sharedInstance.image(category.image) { (error, image) in
                    if error == nil{
                        DispatchQueue.main.async(execute: {
                            self.truckCategory.image = image
                            self.truckCategory.addShadow()
                        })
                    }
                }
            }
        }
        
        truckOpen.backgroundColor = UIColor.appRed()
        if (rating.truck.isOpen()){
            truckOpen.backgroundColor = UIColor.appGreen()
        }
        
        (_, sessionCover) = ServerManager.sharedInstance.image(rating.truck.cover) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    self.truckCover.image = image
                })
            }
        }
        
        (_, sessionAvatar) = ServerManager.sharedInstance.image(rating.truck.avatar) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    self.truckImage.image = image
                })
            }
        }
    }

}
