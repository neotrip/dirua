
//
//  dishCell.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 26/09/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var dishPrice: UILabel!
    
    
    func fill(dish: Dish){
        if let price = dish.price{
            if price.doubleValue > 0 {
                dishPrice.text = "R$ \(price.description)"
            }
        }
        
        ServerManager.sharedInstance.image(dish.image) { (error, image) in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    self.dishImage.image = image
                })
            }
        }
    }
    
}
