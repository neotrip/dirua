//
//  ScheduleCollectionViewCell.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 28/09/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class ScheduleCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var schedules: [Schedule] = []
    
    @IBInspectable public var borderColor:UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable public var borderWidth:CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var cornerRadius:CGFloat = 0.0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = (cornerRadius > 1) ? cornerRadius : cornerRadius * bounds.height
    }
    
    func fill(day: Date){
        weekdayLabel.text = day.weekday()
        dateLabel.text = "\(day.day()) \(day.month())"
    }
    
    func enable(schedule: Schedule){
        weekdayLabel.textColor = UIColor.appGrey()
        dateLabel.textColor = UIColor.appGrey()
        
        self.isUserInteractionEnabled = true
        
        self.schedules.append(schedule)
    }
    
    func disable(){
        weekdayLabel.textColor = UIColor.appLightGrey()
        dateLabel.textColor = UIColor.appLightGrey()
        
        self.isUserInteractionEnabled = false
        
        self.schedules = []
    }
    
}
