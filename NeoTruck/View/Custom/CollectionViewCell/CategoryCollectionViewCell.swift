//
//  CategoryCell.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 30/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: CollectionViewCell {
    
    @IBOutlet weak var backgroundImageView: ImageView!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var sessionImage: URLSessionDataTask?
    
    func fill(_ category: Category){
        
        (_, sessionImage) = ServerManager.sharedInstance.image(category.image) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    self.categoryImageView.image = image
                    self.categoryImageView.addShadow()
                })
            }
        }
    }
}
