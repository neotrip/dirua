//
//  TriangleView.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 10/10/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class TriangleView: UIView {
    let maskLayer = CAShapeLayer()

    public override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: bounds.height))
        path.addLine(to: CGPoint(x: bounds.width/2, y: 0))
        path.addLine(to: CGPoint(x: bounds.width, y: bounds.height))
        path.close()
        
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }

}
