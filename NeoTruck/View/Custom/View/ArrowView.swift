//
//  ArrowView.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 04/10/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class ArrowView: View {
    
    let maskLayer = CAShapeLayer()
    let container = CALayer()
    
    @IBInspectable public var width:CGFloat = 0
    @IBInspectable public var height:CGFloat = 0
    public override var backgroundColor: UIColor? {
        didSet {
            container.backgroundColor = backgroundColor?.cgColor
            super.backgroundColor = nil
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0.0, y: 0))
        path.addLine(to: CGPoint(x: 0.0, y: bounds.height - height))
        path.addLine(to: CGPoint(x: bounds.width/2 - width/2, y: bounds.height - height))
        path.addLine(to: CGPoint(x: bounds.width/2, y: bounds.height))
        path.addLine(to: CGPoint(x: bounds.width/2 + width/2, y: bounds.height - height))
        path.addLine(to: CGPoint(x: bounds.width, y: bounds.height - height))
        path.addLine(to: CGPoint(x: bounds.width, y: 0))
        path.close()
        
        let pathShadow = UIBezierPath()
        pathShadow.move(to: CGPoint(x: distanceX.x, y: distanceY.x))
        pathShadow.addLine(to: CGPoint(x: distanceX.x, y: bounds.height - height + distanceY.y))
        pathShadow.addLine(to: CGPoint(x: bounds.width/2 - width/2, y: bounds.height - height + distanceY.y))
        pathShadow.addLine(to: CGPoint(x: bounds.width/2, y: bounds.height + distanceY.y))
        pathShadow.addLine(to: CGPoint(x: bounds.width/2 + width/2, y: bounds.height - height + distanceY.y))
        pathShadow.addLine(to: CGPoint(x: bounds.width + distanceX.y, y: bounds.height - height + distanceY.y))
        pathShadow.addLine(to: CGPoint(x: bounds.width + distanceX.y, y: distanceY.x))
        pathShadow.close()
        
        layer.shadowPath = pathShadow.cgPath
        maskLayer.path = path.cgPath
        
        container.frame = bounds
        container.mask = maskLayer
        
        layer.insertSublayer(container, at: 0)
    }

}
