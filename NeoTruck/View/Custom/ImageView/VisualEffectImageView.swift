//
//  VisualEffectImageView.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 9/15/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class VisualEffectImageView: ImageView {
    let visualEffectView = UIVisualEffectView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        visualEffectView.effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        visualEffectView.alpha = 0.0
        addSubview(visualEffectView)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        visualEffectView.frame = bounds
    }
    
    
}
