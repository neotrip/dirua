//
//  VisualEffectGradientImageView.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 23/11/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class VisualEffectGradientImageView: ImageView {
    let gradientLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        gradientLayer.removeFromSuperlayer()
        
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.opacity = 0.7
        layer.addSublayer(gradientLayer)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
    
    
}
