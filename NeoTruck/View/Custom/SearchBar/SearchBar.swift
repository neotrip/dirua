//
//  SearchBar.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 04/10/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class SearchBar: UISearchBar {
    
    @IBInspectable public var borderColor:UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable public var borderWidth:CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

}
