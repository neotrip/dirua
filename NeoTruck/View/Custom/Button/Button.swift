//
//  Button.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 9/15/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class Button: UIButton {
    
    @IBInspectable public var borderColor:UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable public var borderWidth:CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable public var distanceX:CGPoint = CGPoint.zero {
        didSet {
            
        }
    }
    @IBInspectable public var distanceY:CGPoint = CGPoint.zero {
        didSet {
            
        }
    }
    
    @IBInspectable public var shadowOpacity:Float = 0.0 {
        didSet {
            if (layer.shadowOpacity != shadowOpacity) {
                let animation:CABasicAnimation = CABasicAnimation(keyPath: "shadowOpacity")
                
                animation.duration = 0.1
                animation.fromValue = layer.shadowOpacity
                animation.toValue = shadowOpacity
                
                layer.add(animation, forKey: "shadowOpacity")
                layer.shadowOpacity = shadowOpacity
            }
            
        }
    }
    @IBInspectable public var shadowColor:UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    @IBInspectable public var shadowRadius:CGFloat = 0.0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    @IBInspectable public var shadowOffset:CGSize = CGSize(width: 0, height: 0) {
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable public var cornerRadius:CGFloat = 0.0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = (cornerRadius > 1) ? cornerRadius : cornerRadius * bounds.height
        
        if (shadowOpacity != 0) {
            let pathShadow = UIBezierPath()
            pathShadow.move(to: CGPoint(x: distanceX.x, y: distanceY.x))
            pathShadow.addLine(to: CGPoint(x: distanceX.x, y: bounds.height + distanceY.y))
            pathShadow.addLine(to: CGPoint(x: bounds.width + distanceX.y, y: bounds.height + distanceY.y))
            pathShadow.close()
            
            layer.shadowPath = pathShadow.cgPath
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
