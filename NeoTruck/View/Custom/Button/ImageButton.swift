//
//  ImageButton.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 9/15/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

@IBDesignable public class ImageButton: Button {
    @IBInspectable public var assetImage:String?
    
    var stateNormal:UIImage?
    var stateSelected:UIImage?
    
    public var select:Bool = false
    
    public func setSelected(bool:Bool, truck: String) {
        if (bool) {
            ServerManager.sharedInstance.wishlist(idAddTruck: truck, completion: { (error, success) in
                if success == true && error == nil{
                    DispatchQueue.main.async(execute: {
                        self.select = bool
                        self.setImage(self.stateSelected, for: UIControlState.normal)
                        ServerManager.sharedInstance.wishlist()
                    })
                }
            })
        } else {
            print("deletando")
            
            ServerManager.sharedInstance.wishlist(idDeleteTruck: truck, completion: { (error, success) in
                if success == true && error == nil{
                    DispatchQueue.main.async(execute: {
                        self.select = bool
                        self.setImage(self.stateNormal, for: UIControlState.normal)
                        ServerManager.sharedInstance.wishlist()
                    })
                }
            })
        }
    }
    
    var size:CGSize = CGSize.zero
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        if (size != bounds.size) {
            size = bounds.size
            
            if let assetImage = assetImage {
                switch assetImage {
                case "wishlist":
                    
//                    stateNormal = Assets.wishlist(size: bounds.size, color: UIColor.white, lineWidth: 2)
//                    stateSelected = Assets.wishlist(size: bounds.size, color: UIColor.white, lineWidth: 0)
                    
                    stateNormal = #imageLiteral(resourceName: "wishlist")
                    stateSelected = #imageLiteral(resourceName: "wishlisted")
                    
                    setImage(stateNormal, for: UIControlState.normal)
                case "leftarrow":
                    setImage(Assets.leftarrow(size: bounds.size, color: UIColor.white, lineWidth: 2), for: UIControlState.normal)
                case "rightarrow":
                    setImage(Assets.rightarrow(size: bounds.size, color: UIColor.white, lineWidth: 2), for: UIControlState.normal)
                default:
                    break
                }
            }
        }
        
        
    }
    
    
}
