//
//  MapView.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 29/09/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import MapKit

@IBDesignable public class MapView: MKMapView {

    @IBInspectable public var borderColor:UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable public var borderWidth:CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var cornerRadius:CGFloat = 0.0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = (cornerRadius > 1) ? cornerRadius : cornerRadius * bounds.height
    }
    
    func changeRegion(center: CLLocationCoordinate2D){
        let radius: Double = 0.05
        
        let span = MKCoordinateSpanMake(radius*0.5, radius*0.5)
        let region = MKCoordinateRegionMake(center, span)
        self.setRegion(region, animated: true)
    }
    
    func removeAllAnnotations() {
        self.removeAnnotations(self.annotations)
    }
    
    func location(foodtruckSchedule: Schedule, image: String?) {
        if let coordinate = foodtruckSchedule.coordinate {
            self.addAnnotation(FoodTruckAnnotation(coordinate: coordinate, title: "Abre as \(foodtruckSchedule.from!.hour())", subtitle: "", image: image))
            changeRegion(center: coordinate)
        }
    }

}
