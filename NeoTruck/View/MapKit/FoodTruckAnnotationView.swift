//
//  FTAnnotationView.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 25/07/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import MapKit

class FoodTruckAnnotationView: MKAnnotationView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        let ftAnnotation = annotation as! FoodTruckAnnotation
        
        frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        layer.cornerRadius = bounds.height/2
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 1.0
        let imageView = UIImageView()
        (_, _) = ServerManager.sharedInstance.image(ftAnnotation.truckImage) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    imageView.image = image
                })
            }
        }
        imageView.frame = frame
        imageView.layer.cornerRadius = imageView.bounds.height/2
        imageView.layer.masksToBounds = true
        addSubview(imageView)
        
        canShowCallout = true
    
        self.backgroundColor = UIColor.appRed()
        
        
        self.leftCalloutAccessoryView = UIButton(type: .infoDark)
    }

}
