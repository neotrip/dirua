//
//  FTAnnotation.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 25/07/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import MapKit

class FoodTruckAnnotation: NSObject, MKAnnotation {

    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    var truckImage: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, image: String?){
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        self.truckImage = image
    }
    
}
