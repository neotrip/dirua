//
//  Config.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 25/07/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

let FOOD_TRUCK_CELL_IDENTIFIER = "foodTruckCell"
let RATING_CELL_IDENTIFIER = "ratingCell"
let SCHEDULE_CELL_IDENTIFIER = "scheduleCell"
let CATEGORY_CELL_IDENTIFIER = "categoryCell"

let openImage = #imageLiteral(resourceName: "openned")
let closeImage = #imageLiteral(resourceName: "closed")

let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)

let ERROR_422 = NSError(domain: "Not Created", code: 422, userInfo: nil)
let ERROR_500 = NSError(domain: "Internal Server Error", code: 500, userInfo: nil)
let ERROR_400 = NSError(domain: "Username/password incorrect", code: 400, userInfo: nil)
let ERROR_401 = NSError(domain: "Not Deleted", code: 401, userInfo: nil)
let ERROR_404 = NSError(domain: "Not Found", code: 404, userInfo: nil)
let ERROR_403 = NSError(domain: "Forbidden", code: 403, userInfo: nil)

enum PerfilTab: Int {
    case WISHLIST
    case RATING
}

import UIKit

@IBDesignable public class GradientView: View{

    @IBInspectable var startColor: UIColor = UIColor.white {
        didSet{
            setupView()
        }
    }
    
    @IBInspectable var endColor: UIColor = UIColor.black {
        didSet{
            setupView()
        }
    }
    
    
    fileprivate func setupView(){
        
        let colors:Array = [startColor.cgColor, endColor.cgColor]
        gradientLayer.colors = colors
     
        self.setNeedsDisplay()
        
    }
    
    // Helper to return the main layer as CAGradientLayer
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    override open class var layerClass: AnyClass{
        return CAGradientLayer.self
    }

}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
}

extension UIView {
    
    class func mark() -> UIView{
        let mark = UIView()
        mark.bounds.size = CGSize(width: 10, height: 10)
        mark.backgroundColor = UIColor.white
        
        return mark
    }
    
    func circle(){
        self.corner(cornerRadius: self.frame.size.height/2)
    }
    
    func corner(cornerRadius: CGFloat){
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
 
    func removeLoading(){
        for view in self.subviews {
            if view is UIActivityIndicatorView{
                let view = view as! UIActivityIndicatorView
                view.stopAnimating()
                view.removeFromSuperview()
            }
        }
    }
    
    func startLoading(style: UIActivityIndicatorViewStyle){
        removeLoading()
        
        let loading = UIActivityIndicatorView(activityIndicatorStyle: style)
        loading.frame = self.bounds
        loading.startAnimating()
        self.addSubview(loading)
    }
    
    func stopLoading(){
        removeLoading()
    }
    
    class func loadFromNibNamed(_ viewName: String, bundle : Bundle? = nil) -> UIView {
        let views = Bundle.main.loadNibNamed("CustomView", owner: nil, options: nil)
        for view in views!{
            if ((view as AnyObject).restorationIdentifier == viewName){
                return view as! UIView
            }
        }
        return UIView()
    }
    
    func addParallaxToView() {
        let amount = 100
        
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        self.addMotionEffect(group)
    }
    
}

extension UIImageView {
    
//    func blur(){
//        for subview in self.subviews{
//            if subview is UIVisualEffectView  {
//                subview.removeFromSuperview()
//            }
//        }
//        
//        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = self.bounds
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        
//        self.addSubview(blurEffectView)
//    }
    
    
    func tintImageWithColor(color: UIColor){
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
    func addShadow(){
        if let image = self.image {
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let context = CGContext(data: nil, width: Int(image.size.width+6), height: Int(image.size.height+6), bitsPerComponent: image.cgImage!.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
            
            context.setShadow(offset: CGSize(width: 2, height: -2), blur: 3, color: UIColor.black.cgColor)
            context.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
            
            let shadowedImage = context.makeImage()
            let image = UIImage(cgImage: shadowedImage!)
            
            self.image = image
        }
    }
    

}





extension UIColor{

    class func appRed()->UIColor{
        return UIColor(red: 206/255, green: 66/255, blue: 46/255, alpha: 1)
    }
    
    class func appYellow()->UIColor{
        return UIColor(red: 238/255, green: 200/255, blue: 88/255, alpha: 1)
    }
    
    class func appBlue()->UIColor{
        return UIColor(red: 107/255, green: 216/255, blue: 183/255, alpha: 1)
    }
    
    class func appGreen()->UIColor{
        return UIColor(red: 199/255, green: 210/255, blue: 153/255, alpha: 1)
    }
    
    class func appBrown()->UIColor{
        return UIColor(red: 93/255, green: 67/255, blue: 64/255, alpha: 1)
    }
    
    class func appGrey()->UIColor{
        return UIColor(red: 65/255, green: 70/255, blue: 77/255, alpha: 1)
    }
    
    class func appLightGrey()->UIColor{
        return UIColor(red: 128/255, green: 138/255, blue: 152/255, alpha: 1)
    }
    
    class func colorByRate(_ value:CGFloat)->UIColor{
    
        if value <= 3 {return UIColor.appRed()}
    
        if value <= 7 {return UIColor.appYellow()}
        
        return UIColor.appGreen()
    }
    
    
}

extension UIFont{

    class func kenyaCoffeeRG(_ size:CGFloat)->UIFont{
        return UIFont(name: "KenyanCoffeeRg-Regular", size: size)!
    }
    
//    class func kenyaCoffeeRG2(_ size:CGFloat)->UIFont{
//        return UIFont(name: "KenyanCoffeeRg-Regular", size: size)!
//    }
//    
//    class func kenyaCoffeeRG3(_ size:CGFloat)->UIFont{
//        return UIFont(name: "KenyanCoffeeRg-Regular", size: size)!
//    }
//    
//    class func kenyaCoffeeRG4(_ size:CGFloat)->UIFont{
//        return UIFont(name: "KenyanCoffeeRg-Regular", size: size)!
//    }
    
    
}

extension UILabel {
    
    func keynanRGLabel(sizeFont: Double) {
        self.font =  UIFont(name: UIFont.kenyaCoffeeRG(CGFloat(sizeFont)).fontName, size: CGFloat(sizeFont))!
        self.sizeToFit()
    }
    
//    func keynanRGLabel2(sizeFont: Double) {
//        self.font =  UIFont(name: UIFont.kenyaCoffeeRG(CGFloat(sizeFont)).fontName, size: CGFloat(sizeFont))!
//        self.sizeToFit()
//    }
//    
//    func keynanRGLabel3(sizeFont: Double) {
//        self.font =  UIFont(name: UIFont.kenyaCoffeeRG(CGFloat(sizeFont)).fontName, size: CGFloat(sizeFont))!
//        self.sizeToFit()
//    }
    
}

extension Date {
    
    func string() -> String {
        let format:DateFormatter = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
        return format.string(from: self)
    }
    
    static func date(_ string:String) -> Date? {
        let format:DateFormatter = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
        return format.date(from: string)
    }
    
    func daysBetween(to: Date) -> Int{
        return Calendar.current.dateComponents([.day], from: self, to: to).day!
    }
    
    func sameDate(from: Date, to: Date) -> Bool{
        for i in 0...from.daysBetween(to: to){
            let dayAux = from.addDay(days: i)
            if (self.day() == dayAux.day() && self.month() == dayAux.month() && self.year() == dayAux.year()) {
                return true
            }
        }
        
        return false
    }
    
    func year() -> String {
        let calendar = NSCalendar.current
        let componentsYear = calendar.component(.year, from: self)
        
        return "\(componentsYear)"
    }
    
    func day() -> String {
        let calendar = NSCalendar.current
        let componentsDay = calendar.component(.day, from: self)

        return "\(componentsDay)"
    }
    
    func month() -> String {
        let calendar = NSCalendar.current
        let componentsMonth = calendar.component(.month, from: self)
        
        switch componentsMonth {
        case 1:
            return "Jan"
        case 2:
            return "Fev"
        case 3:
            return "Mar"
        case 4:
            return "Abr"
        case 5:
            return "Mai"
        case 6:
            return "Jun"
        case 7:
            return "Jul"
        case 8:
            return "Ago"
        case 9:
            return "Set"
        case 10:
            return "Out"
        case 11:
            return "Nov"
        case 12:
            return "Dez"
        default:
            return "Dez"
        }
    }
    
    func show() -> String{
        return "\(self.day()) \(self.month()) \(self.year())"
    }
    
    func addDay(days: Int) -> Date{
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func weekday() -> String {
        let calendar = NSCalendar.current
        let components = calendar.component(.weekday, from: self)
        
        switch components {
        case 1:
            return "Dom"
        case 2:
            return "Seg"
        case 3:
            return "Ter"
        case 4:
            return "Qua"
        case 5:
            return "Qui"
        case 6:
            return "Sex"
        case 7:
            return "Sab"
        default:
            return "Dom"
        }
    }
    
    func hour() -> String {
        let calendar = NSCalendar.current
        let components = calendar.component(.hour, from: self)
        
        return "\(components)h"
    }
    
}

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard(panGesture: UIPanGestureRecognizer) {
        view.endEditing(true)
    }
}
