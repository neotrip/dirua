//
//  ScrollViewController.swift
//  NeoTruck
//
//  Created by Carlos Henrique Cayres on 8/31/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class ScrollViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIImageView!
    
    var truck:Truck!
    var page :FoodTruckController?
    
    @IBOutlet weak var scheduleTableViewHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.image = UIImage(named: "DefaultCover")
        headerView.addParallaxToView()
        

        ServerManager.sharedInstance.truck(id: truck.id) { (error, truck) in
            if (error == nil && truck != nil){
                self.truck = truck
                ServerManager.sharedInstance.image(self.truck.cover, completion: { (error, image) in
                    if error == nil{
                        DispatchQueue.main.async(execute: {
                            self.headerView.image = image
                        })
                    }
                })
            }
        }
    }
    
    @IBOutlet weak var scheduleTableView: UITableView!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scheduleTableViewHeightConstraint.constant = scheduleTableView.contentSize.height
    }
    
    func configScroll(){
        
        if truck != nil{
            
            let position = CGPoint(x: 0, y: headerView.frame.size.height)
            
            print(position)
            
            page = UIStoryboard.foodTruckViewController()
            page!.truck = self.truck
            page?.finding()
            page!.view.frame.size.width = self.view.frame.size.width
            page!.view.frame.origin = CGPoint(x: 0, y: self.view.frame.size.height - (page?.hearderHeight())!)
            page!.view.backgroundColor = UIColor.appLightGrey()
            page!.view.layoutSubviews()
            page!.view.updateConstraints()
            
            
            let height = headerView.frame.size.height + (page?.view.frame.size.height)! // somar com o tamanho da foodtruckcontroller
            let width = self.view.frame.size.width

            scrollView.contentSize = CGSize(width: width, height: height)
            
            
            self.scrollView.addSubview((page?.view)!)
            
        }
        
    
    }
    
    @IBAction func ratingAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "showRating", sender: self.truck)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is RatingController{
        
            let vc = segue.destination as! RatingController
            vc.truck =  sender as? Truck
        
        }
    }
    
    

}

extension ScrollViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleCell", for: indexPath)
        return cell
    }
    
}

extension ScrollViewController : UIScrollViewDelegate{

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let height = headerView.bounds.height
        
        if (offset < 0){
            let scale:CGFloat = 1 - offset/height
            
            var transform = CATransform3DScale(CATransform3DIdentity, scale, scale, 0)
            transform = CATransform3DTranslate(transform, 0, (offset/scale)/2, 0)
            
            headerView.layer.transform = transform
            
        }
        
    }
    
}
