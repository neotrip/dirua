//
//  CatalogController.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 30/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class CatalogController: UIViewController {
    let refreshControl:UIRefreshControl = UIRefreshControl()
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var headerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var foodTrucksTableView: UITableView!
    @IBOutlet weak var categoryLabel: UILabel!

    @IBOutlet weak var tableViewBottomLayoutConstraint: NSLayoutConstraint!
    var trucks: [Truck] = []
    var filterTrucks: [Truck] = []
    var searchActive: Bool = false
    
    var selectedCategory:IndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        foodTrucksTableView.addSubview(refreshControl)
        foodTrucksTableView.setContentOffset(CGPoint(x: 0, y: 43), animated: true)
        
        refreshControl.subviews[0].transform = CGAffineTransform.init(translationX: 0, y: 120+43)
        refreshControl.addTarget(self, action: #selector(CatalogController.refreshing), for: .valueChanged)
        
        print("Pegando categorias")
        categoriesCollectionView.startLoading(style: .white)
        ServerManager.sharedInstance.categories { (error, categories) in
            if error == nil{
                print("Pegou categorias")
                ServerManager.sharedInstance.categories = [Category.all()] + categories
                
                DispatchQueue.main.async(execute: {
                    self.categoriesCollectionView.reloadData()
                    self.categoriesCollectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition.left)
                    
                    self.refreshing()
                })
            }
            
            DispatchQueue.main.async(execute: {
                self.categoriesCollectionView.stopLoading()
            })
        }
        
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.enablesReturnKeyAutomatically = false
        
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillAppear), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillDisappear), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        searchBar.frame.origin.y = min(0, foodTrucksTableView.contentOffset.y)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is FoodTruckController{
            let vc  = segue.destination as! FoodTruckController
            let truck = sender as! Truck
            vc.truck = truck
        }
    }
    
    
    
    
    func refreshing(){
        foodTrucksTableView.startLoading(style: .gray)
        ServerManager.sharedInstance.trucks({ (error, trucks) in
            if error == nil{
                ServerManager.sharedInstance.trucks = trucks
                for truck in ServerManager.sharedInstance.trucks{
                    truck.fillCategory()
                }
                
                self.opennedTrucks()
            }
            
            DispatchQueue.main.async(execute: {
                self.foodTrucksTableView.stopLoading()
            })
        })
        
    }
    
    func opennedTrucks(){
        if (ServerManager.sharedInstance.trucks.count > 0){
            ServerManager.sharedInstance.trucks(time: nil) { (error, trucks) in
                if error == nil {
                    let opennedTrucks = trucks
                    for opennedTruck in opennedTrucks{
                        for truck in ServerManager.sharedInstance.trucks{
                            if (truck.id == opennedTruck.id){
                                truck.schedules = opennedTruck.schedules
                                break
                            }
                        }
                    }
                    
                    DispatchQueue.main.async(execute: {
                        Truck.sort()
                        
                        if (self.selectedCategory.row != 0){
                            self.trucks = Truck.filter(forCategory: ServerManager.sharedInstance.categories[self.selectedCategory.row])
                        } else {
                            self.trucks = ServerManager.sharedInstance.trucks
                        }
                        
                        self.foodTrucksTableView.reloadData()
                        self.refreshControl.endRefreshing()
                    })
                } else {
                    Truck.sort()
                    
                    if (self.selectedCategory.row != 0){
                        self.trucks = Truck.filter(forCategory: ServerManager.sharedInstance.categories[self.selectedCategory.row])
                    } else {
                        self.trucks = ServerManager.sharedInstance.trucks
                    }
                }
            }
        }
    }
    
}

extension CatalogController: UITableViewDataSource, UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView == foodTrucksTableView) {
            headerViewTopConstraint.constant = 43 - scrollView.contentOffset.y
            view.layoutIfNeeded()
            
            if (scrollView.contentOffset.y < 0) {
                searchBar.subviews[0].transform = CGAffineTransform.init(translationX: 0, y: scrollView.contentOffset.y)
            } else {
                searchBar.subviews[0].transform = CGAffineTransform.identity
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchActive){
            return 1 + filterTrucks.count
        }
        return 1 + trucks.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            return headerViewHeightConstraint.constant
        } else {
            return 200
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            return cell
        } else {
            let truckCell = tableView.dequeueReusableCell(withIdentifier: FOOD_TRUCK_CELL_IDENTIFIER) as! FoodTruckTableViewCell
            if (searchActive && filterTrucks.count > 0){
                truckCell.fill(filterTrucks[(indexPath as NSIndexPath).row-1])
            } else {
                truckCell.fill(trucks[(indexPath as NSIndexPath).row-1])
            }
            
            return truckCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row != 0) {
            let selectedTruck: Truck!
            if (searchActive && filterTrucks.count > 0){
                selectedTruck = filterTrucks[indexPath.row-1]
            } else {
                selectedTruck = trucks[indexPath.row-1]
            }
            
            performSegue(withIdentifier: "showTruck", sender: selectedTruck)
        }
    }
    
}

extension CatalogController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ServerManager.sharedInstance.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CATEGORY_CELL_IDENTIFIER, for: indexPath) as! CategoryCollectionViewCell
        cell.fill(ServerManager.sharedInstance.categories[indexPath.row])
        
        if (selectedCategory.item == indexPath.item) {
            cell.backgroundImageView.alpha = 0.0
            cell.categoryImageView.tintImageWithColor(color: UIColor.appGrey())
        } else {
            cell.backgroundImageView.alpha = 1.0
        }
        
        if (indexPath.item == 0) {
            cell.titleLabel.isHidden = false
        } else {
            cell.titleLabel.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell{
            cell.fill(ServerManager.sharedInstance.categories[indexPath.row])
            
            UIView.animate(withDuration: 0.2) {
                cell.backgroundImageView.alpha = 1.0
            }
            
            cell.titleLabel.textColor = UIColor.white
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchActive = false
        
        let cell = collectionView.cellForItem(at: indexPath) as! CategoryCollectionViewCell
        selectedCategory = indexPath
        UIView.animate(withDuration: 0.2) {
            cell.backgroundImageView.alpha = 0.0
            cell.categoryImageView.tintImageWithColor(color: UIColor.appGrey())
        }
        
        categoryLabel.text = ServerManager.sharedInstance.categories[indexPath.row].name
        if (selectedCategory.row != 0){
            self.trucks = Truck.filter(forCategory: ServerManager.sharedInstance.categories[selectedCategory.row])
        } else {
            cell.titleLabel.textColor = UIColor.appGrey()
            self.trucks = ServerManager.sharedInstance.trucks
        }
        
        self.foodTrucksTableView.reloadData()
    }

}

extension CatalogController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false;
        foodTrucksTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false;
        foodTrucksTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false;
        foodTrucksTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText == ""){
            searchActive = false
        } else {
            searchActive = true
            filterTrucks = self.trucks.filter({ (truck) -> Bool in
                let tmp: NSString = truck.name as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
    //        
    //        if(filterTrucks.count == 0){
    //            searchActive = false;
    //        } else {
    //            searchActive = true;
    //        }
        }
        
        foodTrucksTableView.reloadData()
    }
    
}

extension CatalogController {
    
    func keyboardWillAppear(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            var tabBarHeight:CGFloat = 0
            if let height = self.tabBarController?.tabBar.frame.height {
                tabBarHeight = height
            }
            tableViewBottomLayoutConstraint.constant = keyboardSize.height - tabBarHeight
            UIView.animate(withDuration: 1.0, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func keyboardWillDisappear(notification: NSNotification) {
        tableViewBottomLayoutConstraint.constant = 0
        view.layoutIfNeeded()
    }
    
}
