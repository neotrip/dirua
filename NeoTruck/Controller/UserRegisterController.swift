//
//  UserRegisterController.swift
//  NeoTruck
//
//  Created by Lucas Padilha on 9/1/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class UserRegisterController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var warningLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserRegisterController.hideKeyboard))
        view.addGestureRecognizer(tap)
        
        
    }
    
    func hideKeyboard () {
        view.endEditing(true)
    }
    
    @IBAction func tapRegister(_ sender: UIButton) {
        if (emailTextField.text != nil && usernameTextField.text != nil && passwordTextField.text != nil &&
            emailTextField.text != "" && usernameTextField.text != "" && passwordTextField.text != "") {
            ServerManager.sharedInstance.signup(email: emailTextField.text!, username: usernameTextField.text!, password: passwordTextField.text!, completion: { (error, success) in
                if (error == nil){
                    DispatchQueue.main.async(execute: {
                        self.warningLabel.isHidden = false
                        self.warningLabel.text = "done"
                    })
                }
            })
        }
        else {
            warningLabel.isHidden = false
            warningLabel.text = "complete all fields"
        }
    }
}

extension UserRegisterController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        view.endEditing(true)
    }
}
