//
//  DishesScrollViewController.swift
//  NeoTruck
//
//  Created by Carlos Henrique Cayres on 9/9/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class DishesViewController: UIViewController {
    
    var index:Int!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageView: ImageView!
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewMinBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet var arrowsView: UIView!
    @IBOutlet weak var arrowsViewCenterConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.keynanRGLabel(sizeFont: 40)
        nameLabel.minimumScaleFactor = 30
        
        priceLabel.keynanRGLabel(sizeFont: 50)
        priceLabel.contentScaleFactor = 100
        
        arrowsView.alpha = 0.0
    }
    
    
    var animating:Bool = false
    func animate(withDuration duration: TimeInterval, from:CGFloat, to:CGFloat, completion: ((Bool) -> Void)? = nil) {
        animating = true
        imageView.animate(withDuration: duration, from: from-64, to: to) { bool in
            self.animating = false
        }
    }
    
    func back(withDuration duration: TimeInterval, completion: ((Bool) -> Void)? = nil) {
        animating = true
        imageView.animate(withDuration: duration, from: imageView.layer.cornerRadius, to: imageView.layer.frame.height/2, completion: { bool in
            self.animating = false
            self.imageView.layer.cornerRadius = 0
        })
    }
    
}

extension DishesViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
//        print(offset)
        
        titleViewBottomConstraint.constant = offset + titleView.frame.height
        
//        if (animating) {
////            imageView.transform
//            
//            return
//        }
//        if (offset < 0) {
////            if animating {
////                imageView.transform = CGAffineTransform.identity
////            } else {
////                let scale = 1 - offset/imageView.bounds.height
////                imageView.transform = CGAffineTransform.init(scaleX: scale, y: scale)
////            }
//            imageView.transform = CGAffineTransform.identity
//        } else {
//            imageView.transform = CGAffineTransform.init(translationX: 0, y: -offset/4)
//        }
        
        UIView.animate(withDuration: 0.0) {
            self.view.layoutIfNeeded()
        }
        
    }
}
