//
//  LoginControllerViewController.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 07/10/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ProfileViewController: UIViewController {
    var profilePageViewController:ProfilePageViewController!
    var profileTablesViewController = [UIStoryboard.profileTableViewController(),
                                       UIStoryboard.profileTableViewController()]
    
    @IBOutlet var topImageViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userProfilePictureView: FBSDKProfilePictureView!
    @IBOutlet weak var loginFacebookButton: FBSDKLoginButton!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var markView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet var buttonIndicator1CenterConstraint: NSLayoutConstraint!
    @IBOutlet var buttonIndicator2CenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ratingByMeButton: UIButton!
    @IBOutlet weak var wishListButton: UIButton!
    
    var perfilTab: PerfilTab = .RATING {
        didSet {
            
            if (perfilTab == .RATING){
                profilePageViewController.setViewControllers([profileTablesViewController[0]], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
                
                centerMarkView(constraint:buttonIndicator1CenterConstraint)
            } else {
                profilePageViewController.setViewControllers([profileTablesViewController[1]], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
                
                centerMarkView(constraint:buttonIndicator2CenterConstraint)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(patternImage: UIImage(named: "fundo")!)
        
        if (FBSDKAccessToken.current() != nil && ServerManager.sharedInstance.user != nil){
            updateLayout(status: true)
            addProfilePageViewController()
        } else {
            updateLayout(status: false)
        }
        
    }
    
    func centerMarkView(constraint:NSLayoutConstraint) {
        buttonIndicator1CenterConstraint.isActive = false
        buttonIndicator2CenterConstraint.isActive = false
        constraint.isActive = true
        
        let labelFontBold = UIFont(name: "Avenir Next-Bold", size: 18)
        let labelFont = UIFont(name: "Avenir Next", size: 16)
        if (constraint == buttonIndicator1CenterConstraint) {
            ratingByMeButton.titleLabel?.font = labelFontBold
            wishListButton.titleLabel?.font = labelFont
        } else if (constraint == buttonIndicator2CenterConstraint) {
            ratingByMeButton.titleLabel?.font = labelFont
            wishListButton.titleLabel?.font = labelFontBold
        }
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            
            }, completion: nil)
    }
    
    func updateLayout(status:Bool) {
        topImageViewConstraint.isActive = status
        mainView.alpha = (status) ? 1.0 : 0.0
        settingsButton.alpha = (status) ? 1.0 : 0.0
        
        if (status) {
            userProfilePictureView.profileID = ServerManager.sharedInstance.user.facebook
            usernameLabel.text = ServerManager.sharedInstance.user.username
        } else {
            FacebookManager.sharedInstance.logout()
            userProfilePictureView.profileID = nil
            usernameLabel.text = "Usuário Desconectado"
        }
        
    }
    
    @IBAction func ratingByMe(_ sender: AnyObject) {
        perfilTab = .RATING
    }
    
    @IBAction func wishList(_ sender: AnyObject) {
        perfilTab = .WISHLIST
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is FoodTruckController{
            let vc  = segue.destination as! FoodTruckController
            vc.truck = sender as! Truck
        }
    }
    
    @IBAction func showSettings(_ sender: AnyObject) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: "Sair", style: .destructive) { (action) in
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                self.updateLayout(status: false)
                self.view.layoutIfNeeded()
                
                }, completion: nil)
            
        }
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in }
        
        
        alertController.addAction(action)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

extension ProfileViewController: FBSDKLoginButtonDelegate {
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.updateLayout(status: false)
            self.view.layoutIfNeeded()
            
            
            
            }, completion: nil)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        ServerManager.sharedInstance.facebooksignup(facebook: FacebookManager.sharedInstance.fbID(), facebookToken: FacebookManager.sharedInstance.token(), completion: { (error, success) in
            DispatchQueue.main.async(execute: {
//                self.logged = success
                
                self.addProfilePageViewController()
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                    self.updateLayout(status: true)
                    self.view.layoutIfNeeded()
                    
                    
                    
                    }, completion: nil)
                
                
            })
        })
    }
    
}



extension ProfileViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offset = scrollView.contentOffset.x
//        let width = containerView.bounds.width
//        if (offset < width) {
//            centerMarkView(constraint:buttonIndicator1CenterConstraint)
//        } else if (offset > width) {
//            centerMarkView(constraint:buttonIndicator2CenterConstraint)
//        }
//    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (pageViewController.viewControllers![0] == profileTablesViewController[0]) {
            centerMarkView(constraint: buttonIndicator1CenterConstraint)
        } else {
            centerMarkView(constraint: buttonIndicator2CenterConstraint)
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if (viewController == profileTablesViewController[0]) {
            return nil
        } else {
            return profileTablesViewController[0]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if (viewController == profileTablesViewController[0]) {
            return profileTablesViewController[1]
        } else {
            return nil
        }
    }
    
}

extension ProfileViewController : UIScrollViewDelegate {
    
    func addProfilePageViewController() {
        profilePageViewController = ProfilePageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal)
        
        profilePageViewController.didMove(toParentViewController: self)
        addChildViewController(profilePageViewController)
        
        profilePageViewController.scrollView?.delegate = self
        profilePageViewController.dataSource = self
        profilePageViewController.delegate = self
        
        profileTablesViewController[0].perfilTab = .RATING
        profileTablesViewController[1].perfilTab = .WISHLIST
        
        profilePageViewController.setViewControllers([profileTablesViewController[0]], direction: UIPageViewControllerNavigationDirection.forward, animated: true)
        containerView.addSubview(profilePageViewController.view)
        
        profilePageViewController.view.frame = containerView.bounds
    }
    
}

private extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
    
    class func profileTableViewController() -> ProfileTableViewController {
        return mainStoryboard().instantiateViewController(withIdentifier: "profileTableViewController") as! ProfileTableViewController
    }
    
}
