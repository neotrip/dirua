//
//  NavigationController.swift
//  NeoTruck
//
//  Created by Carlos Henrique Cayres on 9/8/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, UINavigationControllerDelegate {
    var tab:UITabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tab = self.tabBarController as? TabBarController
        self.delegate = self
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        switch viewController {
        case is FoodTruckController:
            self.tabBarController?.tabBar.isHidden = true
        case is DishesViewController:
            self.tabBarController?.tabBar.isHidden = true
        default:
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    func title(_ title: String){
        self.navigationBar.topItem?.title = title
    }

}
