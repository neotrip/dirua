//
//  ScrollViewController.swift
//  NeoTruck
//
//  Created by Carlos Henrique Cayres on 8/31/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import MapKit

protocol RefreshRatingsDelegate {
    func refreshRatings()
}

class FoodTruckController: UIViewController {
    let geoCoder = CLGeocoder()
    
    var _currentpage:Int?
    var currentpage:Int {
        get {
            if let aux = _currentpage {
                return aux
            } else {
                fatalError("vish")
            }
        }
        set {
            _currentpage = newValue
        }
    }
    
    var menuExpanded:Int = 0 // 0: not expanded, 1: expanding, 2: expanded
    var commentsExpanded:Bool = false
    
    let howmanydishes:Int = 10
    
    var truck: Truck!
    var truckRatings: [Rating] = []
    var usernames: [String] = []
    var lastOffset:CGPoint?
    var menuViewController:MenuViewController?
    var firstSchedule = false
    var selectedDatIndexPath: IndexPath!
    
    var pages:[DishesViewController] = [UIStoryboard.dishesScrollViewController(),
                                        UIStoryboard.dishesScrollViewController(),
                                        UIStoryboard.dishesScrollViewController()]
    
    
    /* HEADER */
    @IBOutlet weak var headerView: View!
    @IBOutlet weak var headerViewMaxHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewMinHeightConstraint: NSLayoutConstraint!
    @IBOutlet var nameLabelDistanceConstraint: NSLayoutConstraint!
    
    /* HEADER BUTTONS */
    @IBOutlet weak var backButton: ImageButton!
    
    /* HEADER LABEL */
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descInfoLabel: UILabel!
    @IBOutlet weak var ratingButton: ImageButton!
    @IBOutlet weak var wishlistButton: ImageButton!
    @IBOutlet weak var nameLabelCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var wishlistLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    /* HEADER COVER */
    @IBOutlet weak var coverImageView: VisualEffectImageView!
    @IBOutlet weak var coverImageViewBottomConstraint: NSLayoutConstraint!
    
    /* HEADER AVATAR */
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarImageViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var avatarImageViewBottomConstraint: NSLayoutConstraint!
    
    /* BODY */
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ghostHeaderView: UIView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    
    /* STATUS */
    @IBOutlet weak var truckStatusView: View!
    @IBOutlet weak var truckStatusLabel: UILabel!
    
    /* RATING */
    @IBOutlet weak var ratingTapView: UIView!
    @IBOutlet weak var noEvaluatorsLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var evaluatorsLabel: UILabel!
    
    /* DISHES VIEW */
    @IBOutlet weak var noDishesLabel: UILabel!
    
    @IBOutlet weak var dishesView: UIView!
    @IBOutlet weak var dishesViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var menuView: ArrowView!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet weak var menuCollectionViewHeightConstraint: NSLayoutConstraint!
    
    /* SCHEDULE VIEW */
    @IBOutlet weak var noSchedulesLabel: UILabel!
    
    @IBOutlet weak var schedulesCollectionView: UICollectionView!
    @IBOutlet weak var scheduleMapView: MapView!
    
    @IBOutlet weak var rateTableView: UITableView!
    @IBOutlet weak var rateTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressEffectView: UIVisualEffectView!
    
    func tapMenuView(recognizer:UITapGestureRecognizer) {
        if (menuExpanded == 2) {
            tapDishCollectionView(recognizer: recognizer)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        coverImageView.visualEffectView.alpha = 0.5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabelDistanceConstraint.isActive = false
        
        wishlistButton.isHidden = true
        
        noEvaluatorsLabel.isHidden = true
        
        loading()
        
        let tapGestureMenuView = UITapGestureRecognizer(target: self, action: #selector(tapMenuView))
        menuView.addGestureRecognizer(tapGestureMenuView)
        
        rateTableView.rowHeight = UITableViewAutomaticDimension
        rateTableView.estimatedRowHeight = 70
        
        ratingLabel.keynanRGLabel(sizeFont: 30)
        ratingView.starSize = 20
        
        ratingTapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rating(_:))))
        
        ServerManager.sharedInstance.truck(basicTruck: self.truck) { (error, truck) in
            if error == nil && truck != nil{
                self.truck = truck
                
                DispatchQueue.main.async(execute: {
                    self.fill()
                    ServerManager.sharedInstance.ratings(idTruck: self.truck.id, completion: { (error, ratings) in
                        if error == nil{
                            self.truckRatings = ratings
                            
                            DispatchQueue.main.async(execute: {
                                self.rateTableView.reloadData()
                            })
                        }
                    })
                })
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.rateTableViewHeightConstraint.constant = self.rateTableView.contentSize.height
    }
    
    @IBAction func calll(_ sender: Any) {
        if let phone = truck.phoneNumber{
            let url = URL(string: "tel://\(phone)")
            UIApplication.shared.openURL(url!)
        }
    }
    
    func loading(){
        ServerManager.sharedInstance.image(truck.avatar) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    self.avatarImageView.image = image
                })
            }
        }
        
        ServerManager.sharedInstance.image(truck.cover) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    self.coverImageView.image = image
                })
            }
        }
        
        opennedTruck()
        if let name = truck.name {
            nameLabel.text = name
        }
        
        if let rate = truck.rate {
            ratingLabel.text = String(format: "%.1f", rate)
            
            ratingView.filledColor = UIColor.appYellow()
            ratingView.emptyBorderColor = UIColor.appYellow()
            ratingView.filledBorderColor = UIColor.appYellow()
            ratingView.rating = rate
        } else {
            noEvaluatorsLabel.isHidden = false
        }
        
        if let raters = truck.raters {
            evaluatorsLabel.text = String(format: "%d \((raters==1) ? "avaliador" : "avaliadores")", raters)
            evaluatorsLabel.textColor = UIColor.black
        } else {
            //            evaluatorsLabel.text = "avaliadores insuficientes"
        }
        
        if let category = truck.categories?.first {
            if (category.name != "TODOS"){
                categoryLabel.text = category.name
                (_, _) = ServerManager.sharedInstance.image(category.image) { (error, image) in
                    if error == nil{
                        DispatchQueue.main.async(execute: {
                            self.categoryImageView.image = image
                            self.categoryImageView.addShadow()
                            self.categoryImageView.tintImageWithColor(color: UIColor.appGrey())
                           
            
                        })
                    }
                }
            }
        } else {
            categoryImageView.isHidden = true
            categoryView.isHidden = true
        }
        
    }
    
    func opennedTruck(){
        self.truckStatusView.backgroundColor = UIColor.appRed()
        self.truckStatusLabel.text = "fechado"
        if truck.schedules != nil{
            if (truck.isOpen()){
                self.truckStatusView.backgroundColor = UIColor.appGreen()
                self.truckStatusLabel.text = "aberto"
            }
        }
    }
    
    func fill(){
        if let desc = truck.descInfo{
            descInfoLabel.text = desc
        }
        
        opennedTruck()
        
        wishlistButton.select = false
        wishlistButton.setImage(wishlistButton.stateNormal, for: UIControlState.normal)
        if (ServerManager.sharedInstance.user != nil){
            for wishlistTruck in ServerManager.sharedInstance.wishlists {
                if (wishlistTruck.id == self.truck.id){
                    wishlistButton.select = true
                    wishlistButton.setImage(wishlistButton.stateSelected, for: UIControlState.normal)
                    break
                }
            }
        }
        
        wishlistButton.isHidden = false
        
        if (truck.schedules.count == 0){
            noSchedulesLabel.isHidden = false
            scheduleMapView.alpha = 0.1
            scheduleMapView.isUserInteractionEnabled = false
        }
        
        if (truck.dishes.count == 0){
            noDishesLabel.isHidden = false
        }
        
        if let phone = truck.phoneNumber{
            if (phone != ""){
                phoneLabel.isHidden = false
                phoneButton.isHidden = false
                phoneLabel.text = phone
            } else {
                phoneLabel.isHidden = true
                phoneButton.isHidden = true
            }
        } else {
            phoneLabel.isHidden = true
            phoneButton.isHidden = true
        }
    
        menuCollectionView.reloadData()
        schedulesCollectionView.reloadData()
    }
    
    func myLastRating() -> Date {
        var earlyRating: Date = Date(timeIntervalSince1970: 0)
        for rating in truckRatings{
            if let createdAt = rating.createdAt{
                if (rating.facebook! == ServerManager.sharedInstance.user.facebook! && createdAt > earlyRating){
                    earlyRating = createdAt
                }
            }
        }
        
        return earlyRating
    }
    
    var menuViewHeight:CGFloat {
        get {
            let headerViewHeight = ((headerViewMaxHeightConstraint.multiplier/1.3)-headerViewMinHeightConstraint.multiplier) * view.frame.height
            return view.frame.height - headerViewHeight - menuView.frame.height - 8 + 20
        }
    }
    
    func tapDishCollectionView(recognizer:UITapGestureRecognizer?) {
        if (menuExpanded == 0) {
            let headerViewHeight = ((headerViewMaxHeightConstraint.multiplier/1.3)-headerViewMinHeightConstraint.multiplier) * view.frame.height
            let offset = CGPoint(x: 0, y: menuView.frame.origin.y + ghostHeaderView.frame.height - headerViewHeight)
            
            lastOffset = scrollView.contentOffset
            
            menuExpanded = 1
            dishesViewHeightConstraint.constant = menuViewHeight
            UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.scrollView.setContentOffset(offset, animated: false)
            }) { bool in
                self.menuExpanded = 2
            }
        } else {
            let currentViewController = menuViewController?.viewControllers?.first as? DishesViewController
            
            let currentDish:Int
            if let index = currentViewController?.index {
                currentDish = index
            } else {
                currentDish = 0
            }
            
            for page in pages {
                page.imageView?.transform = CGAffineTransform.identity
                if page != currentViewController {
                    page.view.removeFromSuperview()
                } else {
                    page.view.clipsToBounds = false
                }
            }
            
            let indexPaths = menuCollectionView.indexPathsForVisibleItems
            let menuHeight = menuCollectionViewHeightConstraint.constant
            
            var origin:CGPoint!
            let size:CGSize = CGSize(width: menuHeight, height: menuHeight)
            if (currentDish < indexPaths[0].item) {
                origin = CGPoint(x: 0, y: 0)
                menuCollectionView.scrollToItem(at: IndexPath(item: currentDish, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
            } else if (indexPaths[indexPaths.count-1].item < currentDish) {
                origin = CGPoint(x: view.frame.width-size.width, y: 0)
                menuCollectionView.scrollToItem(at: IndexPath(item: currentDish, section: 0), at: UICollectionViewScrollPosition.right, animated: true)
            } else {
                for current in indexPaths {
                    if (current.item == currentDish) {
                        let cell = menuCollectionView.cellForItem(at: current)!
                        let point = cell.superview!.convert(cell.frame.origin, to: nil)
                        origin = CGPoint(x: point.x, y: 0)
                        break
                    }
                }
            }
            
            if (menuExpanded != 1 && lastOffset != nil) {
                self.scrollView.setContentOffset(lastOffset!, animated: true)
            }
            
            let from = menuViewController!.view.frame
            let width = menuHeight*(from.width/from.height)
            let to = CGRect(x: origin.x+(menuHeight-width)/2, y: menuCollectionView.frame.origin.y, width: width, height: menuHeight)
            
            menuExpanded = 0
            
            menuViewController!.view.clipsToBounds = false
            menuViewController!.view.subviews.first?.clipsToBounds = false
            
            currentViewController?.titleViewBottomConstraint.constant = 0
            currentViewController?.titleViewMinBottomConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                currentViewController?.scrollView.setContentOffset(CGPoint(x: 0, y: -128), animated: false)
                currentViewController?.view.layoutIfNeeded()
            }) { bool in
                
                currentViewController?.titleView.isHidden = true
                currentViewController?.back(withDuration: 0.5)
                self.menuViewController!.back(withDuration: 0.5, to: to) { bool in
                    currentViewController?.view.clipsToBounds = true
                    currentViewController?.titleView.isHidden = false
                    self.menuViewController?.view.removeFromSuperview()
                    self.menuViewController = nil

                    UIView.animate(withDuration: 0.3, animations: {
                        self.dishesViewHeightConstraint.constant = menuHeight
                        self.view.layoutIfNeeded()
                    })

                }

                
                
            }
        }
    }
    
    @IBAction func rating(_ sender: AnyObject) {
        if ServerManager.sharedInstance.user != nil{
            if (myLastRating().daysBetween(to: Date()) > 2){
                performSegue(withIdentifier: "showRating", sender: self.truck)
            } else {
                showCantRateAlert()
            }
        } else {
            showLoginAlert()
        }
    }
    
    @IBAction func back(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func wish(_ sender: ImageButton) {
        if ServerManager.sharedInstance.user != nil{
            sender.setSelected(bool: !sender.select, truck: truck.id)
        } else {
            showLoginAlert()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is RatingController{
            let vc  = segue.destination as! RatingController
            vc.truck = sender as! Truck
            vc.delegate = self
        }
    }
    
    func showLoginAlert(){
        let alertController = UIAlertController(title: "Quem é você?", message: "Você precisa logar", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Entendido", style: .cancel) { (action) in }
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
    }
    
    func showCantRateAlert(){
        let alertController = UIAlertController(title: "Você já avaliou esse foodtruck recentemente", message: "", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Entendido", style: .cancel) { (action) in }
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
    }
    
}

extension FoodTruckController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.scrollView == scrollView) {
            
            let offset = scrollView.contentOffset.y
            let height = coverImageView.bounds.height
            
            let scale:CGFloat = 1 - offset/height
            let percentage = 1 - (max(0, scale)) / 1
            
            print(percentage)
            
            coverImageView.visualEffectView.alpha = (percentage < 0) ? max(0, percentage + 0.5) : 0.5 + percentage/2
            headerView.shadowOpacity = (percentage < 1) ? 0 : 0.3

            headerViewMaxHeightConstraint.constant = -offset
            nameLabelCenterConstraint.constant = percentage * 10
            avatarImageViewCenterConstraint.constant = -max(0, percentage) * ((view.frame.width-avatarImageView.frame.width)/2 - backButton.frame.width - 2*backButton.frame.origin.x)
            
            if (offset < 0) {
                nameLabelDistanceConstraint.isActive = false
                
                rateLabel.isHidden = false
                wishlistLabel.isHidden = false
                
                avatarImageViewBottomConstraint.constant = -20
                coverImageViewBottomConstraint.constant = -offset
                
                var transform = CATransform3DScale(CATransform3DIdentity, scale, scale, 0)
                transform = CATransform3DTranslate(transform, 0, -(offset/scale)/2, 0)
                coverImageView.layer.transform = transform
            } else if (offset == 0){
                nameLabelDistanceConstraint.isActive = false
                
                rateLabel.isHidden = false
                wishlistLabel.isHidden = false
            } else {
                nameLabelDistanceConstraint.isActive = true
                
                rateLabel.isHidden = true
                wishlistLabel.isHidden = true
                
                avatarImageViewBottomConstraint.constant = offset/2 - 20
                coverImageViewBottomConstraint.constant = 0
                coverImageView.layer.transform = CATransform3DIdentity
            }
            
            UIView.animate(withDuration: 0.0) {
                self.view.layoutIfNeeded()
            }
            
            if (menuExpanded == 2) {
                menuExpanded = 1
                tapDishCollectionView(recognizer: nil)
            }
        } else if (menuCollectionView == scrollView) {
        } else if (schedulesCollectionView == scrollView) {
            
        } else {
            if (truck.dishes.count > 1){
                let scrolleft = scrollView.contentOffset.x < scrollView.frame.width
                let distance = (scrollView.contentOffset.x - scrollView.frame.width)/4
                
                let page = pages[currentpage]
                page.imageView.transform = CGAffineTransform.init(translationX: distance, y: page.imageView.transform.ty)
                
                
                if (scrolleft) {
                    let index = (currentpage-1 + truck.dishes.count) % truck.dishes.count
                    let page = pages[(index)%pages.count]
                    
                    if let imageView = page.imageView {
                        imageView.transform = CGAffineTransform.init(translationX: scrollView.frame.width/4+distance, y: imageView.transform.ty)
                    }
                    
                    
    //                page.imageView?.transform
                } else {
                    let index = (currentpage+1 + truck.dishes.count) % truck.dishes.count
                    let page = pages[(index)%pages.count]
                    
                    if let imageView = page.imageView {
                        imageView.transform = CGAffineTransform.init(translationX: -(scrollView.frame.width/4-distance), y: imageView.transform.ty)
                    }
                }
            }
        }
    }
    
}

extension FoodTruckController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let foodtruckAnnotation = annotation as! FoodTruckAnnotation
        let foodtruckAnnotationView = FoodTruckAnnotationView(annotation: foodtruckAnnotation, reuseIdentifier: foodtruckAnnotation.title)
        foodtruckAnnotationView.annotation = foodtruckAnnotation
        
        return foodtruckAnnotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if (control == view.leftCalloutAccessoryView){
            let foodtruckAnnotationView = view as! FoodTruckAnnotationView
            let placemark = MKPlacemark(coordinate: foodtruckAnnotationView.annotation!.coordinate, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = truck.name
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        addressEffectView.isHidden = true
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let foodtruckAnnotationView = view as! FoodTruckAnnotationView
        let foodtruckAnnotation = foodtruckAnnotationView.annotation as! FoodTruckAnnotation
        
        geoCoder.cancelGeocode()
        geoCoder.reverseGeocodeLocation(CLLocation(latitude: foodtruckAnnotation.coordinate.latitude, longitude: foodtruckAnnotation.coordinate.longitude), completionHandler: {(placemarks, error) ->
            Void in
            self.addressEffectView.isHidden = false
            if (error != nil) {
                self.addressLabel.text = "Não há endereço"
                return
            }
            
            if placemarks!.count > 0 {
                let placemarkLocation = (placemarks?.first)! as CLPlacemark
                var result = ""
                if let address = placemarkLocation.thoroughfare{
                    result += address
                }
                
                if let number = placemarkLocation.subThoroughfare{
                    result += " - \(number)"
                }
                
                if let city = placemarkLocation.locality{
                    result += " - \(city)"
                }
                
                if let state = placemarkLocation.administrativeArea{
                    result += "/\(state)"
                }
                
                self.addressLabel.text = result
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    
}

extension FoodTruckController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if (collectionView == schedulesCollectionView){
            let width = collectionView.frame.size.width/7
            let height = collectionView.frame.size.height
            
            return CGSize(width: width, height: height)
        } else {
            return CGSize(width: 100, height: 100)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == menuCollectionView) {
            if (truck.dishes.count == 0) {
                return 0
            } else {
                return truck.dishes.count
            }
        } else if (collectionView == schedulesCollectionView) {
            return 7
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == menuCollectionView) {
            let dishCell = collectionView.dequeueReusableCell(withReuseIdentifier: "dishCell", for: indexPath) as! MenuCollectionViewCell
            
            if (truck.dishes.count != 0) {
                dishCell.fill(dish: truck.dishes[indexPath.row])
            }
            
            return dishCell
        } else if (collectionView == schedulesCollectionView) {
            let scheduleCell = collectionView.dequeueReusableCell(withReuseIdentifier: "scheduleCell", for: indexPath) as! ScheduleCollectionViewCell
            
            let cellDay = Date().addDay(days: indexPath.row)
            
            scheduleCell.fill(day: cellDay)
            
            scheduleCell.disable()
            if let schedules = truck.schedules{
                for schedule in schedules{
                    if (cellDay.sameDate(from: schedule.from!, to: schedule.to!)){
                        scheduleCell.enable(schedule: schedule)
                        if (!firstSchedule){
                            firstSchedule = true
                            selectedDatIndexPath = indexPath
                            
                            scheduleMapView.removeAllAnnotations()
                            scheduleMapView.location(foodtruckSchedule: schedule, image: truck.avatar)
                        }
                    }
                }
            }
            
            if let first = scheduleMapView.annotations.first{
                scheduleMapView.selectAnnotation(first, animated: true)
            }
            
            if (selectedDatIndexPath != nil && indexPath == selectedDatIndexPath){
                scheduleCell.backgroundColor = UIColor.appYellow()
            } else {
                scheduleCell.backgroundColor = UIColor.clear
            }
            
            return scheduleCell
        } else {
            fatalError("you little shit")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == menuCollectionView) {
            
            if (truck.dishes.count != 0) {
                tapDishCollectionView(recognizer: nil)
                
                if let cell = collectionView.cellForItem(at: indexPath) {
                    let origin = cell.superview!.convert(cell.frame.origin, to: nil)
                    let size = cell.bounds.size
                    
                    addDishesViewController(index: indexPath.item, from: CGRect(x: origin.x, y: menuCollectionView.frame.origin.y, width: size.width, height: size.height))
                }
                
            }
        } else if (collectionView == schedulesCollectionView) {
            scheduleMapView.removeAllAnnotations()
            for schedule in (collectionView.cellForItem(at: indexPath) as! ScheduleCollectionViewCell).schedules{
                scheduleMapView.location(foodtruckSchedule: schedule, image: truck.avatar)
            }
            
            if let first = scheduleMapView.annotations.first{
                scheduleMapView.selectAnnotation(first, animated: true)
            }
            
            selectedDatIndexPath = indexPath
            collectionView.reloadData()
//            scheduleHourLabel.text = "\(schedule.fromeh p!.fromHour()) - \(schedule.from!.toHour(from: schedule.from!))"
        } else {
            fatalError("you little shit")
        }
    }
    
}

extension FoodTruckController : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! DishesViewController
        return viewControllerFrom(viewController: viewController, atIndex: vc.index-1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let vc = viewController as! DishesViewController
        return viewControllerFrom(viewController: viewController, atIndex: vc.index+1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
//        if (!completed) {
//            return
//        }
        let index = pages.index(of: pageViewController.viewControllers!.first! as! DishesViewController)
        currentpage = index!
    }
    
    func viewControllerFrom(viewController:UIViewController, atIndex:Int) -> DishesViewController {
        let vc:DishesViewController
        
        let index = (atIndex + truck.dishes.count) % truck.dishes.count
        vc = (viewController == pages[index%pages.count]) ? pages[(index+1)%pages.count] : pages[index%pages.count]
        
        
        let _ = vc.view
        vc.scrollView.setContentOffset(CGPoint.zero, animated:false)
        vc.index = index
        vc.nameLabel.text = truck.dishes[index].name
        vc.descriptionLabel.text = truck.dishes[index].info
        if let price = truck.dishes[index].price{
            vc.priceLabel.text = "R$ \(price.description)"
        }
        
        
        ServerManager.sharedInstance.image(truck.dishes[index].image) { (error, image) in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    vc.imageView.image = image
                })
            }
        }
        
        return vc
    }
    
}

extension FoodTruckController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (commentsExpanded){
            return truckRatings.count
        } else {
            if (truckRatings.count > 5){
                return 6
            } else {
                return truckRatings.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (!commentsExpanded && indexPath.row >= 5 && truckRatings.count > 5) {
            let showMoreCell = tableView.dequeueReusableCell(withIdentifier: "show", for: indexPath)
            return showMoreCell
        } else {
            let truckratingCell = tableView.dequeueReusableCell(withIdentifier: "truckratingCell", for: indexPath) as! UserRatingTableViewCell
            truckratingCell.fill(truckRatings[indexPath.row])
            
            return truckratingCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (!commentsExpanded && indexPath.row >= 5 && truckRatings.count > 5) {
            commentsExpanded = !commentsExpanded
            tableView.reloadData()
            self.rateTableViewHeightConstraint.constant = self.rateTableView.contentSize.height
        }
    }
    
}

extension FoodTruckController: RefreshRatingsDelegate {
    
    func refreshRatings() {
        ServerManager.sharedInstance.ratings(idTruck: self.truck.id, completion: { (error, ratings) in
            if error == nil{
                self.truckRatings = ratings
                
                DispatchQueue.main.async(execute: {
                    self.rateTableView.reloadData()
                })
            }
        })
    }
    
}

extension FoodTruckController {
    
    func addDishesViewController(index:Int, from:CGRect) {
        if (menuViewController == nil) {
            let firstViewController = viewControllerFrom(viewController: pages[0], atIndex: index)
            
            menuViewController = MenuViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal)
            menuViewController!.didMove(toParentViewController: self)
            addChildViewController(menuViewController!)
            
            menuViewController!.dataSource = self
            menuViewController!.delegate = self
            menuViewController!.scrollView?.delegate = self
            menuViewController!.setViewControllers([firstViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true)
            
            let index = pages.index(of: menuViewController!.viewControllers!.first! as! DishesViewController)
            currentpage = index!
            
//            menuViewController!.view.backgroundColor = UIColor.black
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapDishCollectionView))
            menuViewController!.view.addGestureRecognizer(tapGesture)
            
            dishesView.addSubview(menuViewController!.view)
            
            let to = CGRect(x: 0, y: 0, width: view.frame.width, height: menuViewHeight)
            firstViewController.view.clipsToBounds = false
            menuViewController!.view.clipsToBounds = false
            menuViewController!.view.subviews.first?.clipsToBounds = false
            
            firstViewController.titleView.isHidden = true
            firstViewController.titleViewBottomConstraint.constant = 0
            firstViewController.titleViewMinBottomConstraint.constant = 0
            firstViewController.arrowsViewCenterConstraint.constant = -5
            
            firstViewController.animate(withDuration: 0.3, from: to.height/2, to: 0.0)
            menuViewController!.animate(withDuration: 0.3, from: from, to:to) { bool in
                self.menuViewController!.view.clipsToBounds = true
                self.menuViewController!.view.subviews.first?.clipsToBounds = true
                
                firstViewController.view.clipsToBounds = true
                firstViewController.titleView.isHidden = false
                firstViewController.titleViewBottomConstraint.constant = firstViewController.titleView.frame.height
                
                
                
                
                UIView.animate(withDuration: 0.2, animations: {
                    firstViewController.arrowsView.alpha = 0.8
                    firstViewController.view.layoutIfNeeded()
                }) { bool in
                    firstViewController.titleViewMinBottomConstraint.constant = firstViewController.titleView.frame.height
                    
                    UIView.animate(withDuration: 0.9, animations: {
                        firstViewController.arrowsView.alpha = 0.0
                    })
                    
                    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                        firstViewController.arrowsViewCenterConstraint.constant = 5
                        firstViewController.view.layoutIfNeeded()
                    }) { _ in
                        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                            firstViewController.arrowsViewCenterConstraint.constant = -5
                            firstViewController.view.layoutIfNeeded()
                        }) { _ in
                            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                                firstViewController.arrowsViewCenterConstraint.constant = 5
                                firstViewController.view.layoutIfNeeded()
                            })
                        }
                    }
                }
                
                
            }
            
            
        }
    }
    
}

private extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
    
    class func dishesScrollViewController() -> DishesViewController {
        return mainStoryboard().instantiateViewController(withIdentifier: "dishesViewController") as! DishesViewController
    }
    
}
