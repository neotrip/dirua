//
//  RatingController.swift
//  NeoTruck
//
//  Created by Carlos Henrique Cayres on 9/8/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class RatingController: UIViewController {
    
    @IBOutlet weak var truckNameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var confirmedButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var commentRatingView: UITextView!
    @IBOutlet weak var ratingModalView: View!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    var delegate: RefreshRatingsDelegate!
    
    var truck: Truck!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        commentRatingView.delegate = self
        truckNameLabel.text = truck.name
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(RatingController.cancelAction(_:)))
        blurView.addGestureRecognizer(tapGesture)
        
        ServerManager.sharedInstance.image(truck.avatar) { (error, image) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    self.truckImageView.image = image
                })
            }
        }

    }
    
    
    @IBAction func confirmedAction(_ sender: UIButton) {
        sender.startLoading(style: .gray)
        ServerManager.sharedInstance.rating(idAddTruck: truck.id, general: NSNumber(value: ratingView.rating), comment: commentRatingView.text) { (error, success) in
            if error == nil{
                DispatchQueue.main.async(execute: {
                    self.delegate.refreshRatings()
                    self.dismiss(animated: true, completion: nil)
                })
            }
            
            DispatchQueue.main.async(execute: {
                sender.stopLoading()
            })
        }
    }
    
    @IBAction func cancelAction(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension RatingController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (self.view.center.y - cancelButton.center.y < 0){
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.view.transform = CGAffineTransform(translationX: 0, y: self.view.center.y - self.cancelButton.center.y )
            })
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        })
        
    }
    
}
