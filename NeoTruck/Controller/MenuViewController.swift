//
//  MenuViewController.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 21/09/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class MenuViewController: UIPageViewController {
    
    var scrollView:UIScrollView! {
        get {
            for subview in view.subviews {
                if subview is UIScrollView {
                    return subview as! UIScrollView
                }
            }
            return nil
        }
    }
    
    func animate(withDuration duration: TimeInterval, from:CGRect, to:CGRect, completion: ((Bool) -> Void)? = nil) {
        view.frame = to
        
        let scale = from.height/(to.height-128)
        var transform = CATransform3DScale(CATransform3DIdentity, scale, scale, 0)
        transform = CATransform3DTranslate(transform,
                                           (((from.width-to.width)/2)+(from.origin.x))/scale,
                                           64+(((from.height-to.height)/2)+(from.origin.y))/scale, 0)
        
        self.view.layer.transform = transform
        UIView.animate(withDuration: duration, animations: {
            self.view.layer.transform = CATransform3DIdentity
            }, completion: completion)
    }
    
    func back(withDuration duration: TimeInterval, to:CGRect, completion: ((Bool) -> Void)? = nil) {
//        view.frame = to
        let from = view.frame

        let scale = to.height/(from.height-128)
        var transform = CGAffineTransform(scaleX: scale, y: scale)
            .translatedBy(x: (((to.width-from.width)/2)+(to.origin.x))/scale,
                          y: 64+(((to.height-from.height)/2)+(to.origin.y))/scale)
        
        
//        var transform = CATransform3DScale(view.layer.transform, scale, scale, 0)
//        transform = CATransform3DTranslate(transform,
//                                           ((from.width-to.size.width)/2-to.origin.x)/scale,
//                                           (((-to.height+from.height)/2)+(from.origin.y))/scale, 0)
//
//        view.layer.transform = transform
        UIView.animate(withDuration: duration, animations: {
//            self.view.layer.transform = transform
            self.view.transform = transform
            
            }, completion: completion)
    }
    
}
