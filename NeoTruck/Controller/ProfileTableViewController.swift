//
//  ProfileTableViewController.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 11/10/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    
    @IBOutlet weak var totalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(ProfileTableViewController.refreshing), for: .valueChanged)
    }
    
    var tableViewObject: [AnyObject] = []
    var perfilTab: PerfilTab = .RATING {
        didSet { self.refreshing() }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.refreshing()
    }
    
    func refreshing(){
        if refreshControl!.isRefreshing{
            refreshControl?.endRefreshing()
        }
        
        tableView.startLoading(style: .gray)
        if (perfilTab == .RATING){
            ServerManager.sharedInstance.rating({ (error, ratings) in
                if (error == nil){
                    for rating in ratings{
                        rating.fillTruck()
                        rating.truck.fillCategory()
                    }
                    
                    self.tableViewObject = ratings
                    
                    self.opennedTrucks()
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.stopLoading()
                })
            })
        } else {
            ServerManager.sharedInstance.wishlist({ (error, trucks) in
                if (error == nil){
                    for truck in trucks{
                        truck.fillCategory()
                    }
                    
                    self.tableViewObject = trucks
                    ServerManager.sharedInstance.wishlists = trucks
                    
                    self.opennedTrucks()
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.stopLoading()
                })
            })
        }
    }
    
    func opennedTrucks(){
        if (self.tableViewObject.count > 0){
            ServerManager.sharedInstance.trucks(time: nil) { (error, trucks) in
                if error == nil {
                    let opennedTrucks = trucks
                    for opennedTruck in opennedTrucks{
                            for object in self.tableViewObject{
                                if (self.perfilTab == .RATING){
                                    if ((object as! Rating).truck.id == opennedTruck.id){
                                        (object as! Rating).truck.schedules = opennedTruck.schedules
                                    }
                                } else {
                                    if ((object as! Truck).id == opennedTruck.id){
                                        (object as! Truck).schedules = opennedTruck.schedules
                                        break
                                    }
                                }
                            }
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                    self.refreshControl?.endRefreshing()
                })
            }
        } else {
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
            })
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var stringAux: String
        if (self.perfilTab == .RATING){
            stringAux = "visitado"
        } else {
            stringAux = "desejado"
        }
        
        if (self.tableViewObject.count != 1){
            stringAux += "s"
        }
        
        self.totalLabel.text = "\(self.tableViewObject.count) \(stringAux)"
        
        return tableViewObject.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if (perfilTab == .WISHLIST){
            let truckCell = tableView.dequeueReusableCell(withIdentifier: FOOD_TRUCK_CELL_IDENTIFIER) as! FoodTruckTableViewCell
            truckCell.fill(tableViewObject[indexPath.row] as! Truck)
            
            cell = truckCell
        } else {
            let ratingCell = tableView.dequeueReusableCell(withIdentifier: RATING_CELL_IDENTIFIER) as! RatingTableViewCell
            ratingCell.fill(tableViewObject[indexPath.row] as! Rating)
            
            cell = ratingCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let truckCell = cell as? FoodTruckTableViewCell {
            truckCell.sessionAvatar?.cancel()
            truckCell.sessionCover?.cancel()
        } else if let ratingCell = cell as? RatingTableViewCell {
            ratingCell.sessionAvatar?.cancel()
            ratingCell.sessionCover?.cancel()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (perfilTab == .WISHLIST){
            let selectedTruck = tableViewObject[indexPath.row] as! Truck
            performSegue(withIdentifier: "showTruck", sender: selectedTruck)
        } else if (perfilTab == .RATING){
            let selectedTruck = tableViewObject[indexPath.row] as! Rating
            performSegue(withIdentifier: "showTruck", sender: selectedTruck.truck)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is FoodTruckController{
            let vc  = segue.destination as! FoodTruckController
            vc.truck = sender as! Truck
        }
    }

}
