//
//  ProfilePageViewController.swift
//  NeoTruck
//
//  Created by Marcos Kobuchi on 11/10/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class ProfilePageViewController: UIPageViewController {
    
    var scrollView:UIScrollView! {
        get {
            for subview in view.subviews {
                if subview is UIScrollView {
                    return subview as! UIScrollView
                }
            }
            return nil
        }
    }
    
}
