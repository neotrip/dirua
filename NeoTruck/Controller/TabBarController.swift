//
//  TabBarController.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 31/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {

    var catalogBarButtonItem: UIBarButtonItem?
    var perfilBarButtonItem: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (FBSDKAccessToken.current() != nil){
            ServerManager.sharedInstance.facebooksignup(facebook: FacebookManager.sharedInstance.fbID(), facebookToken: FacebookManager.sharedInstance.token()) { (error, wishlists) in
                if error == nil {
                    ServerManager.sharedInstance.wishlist()
                }
            }
        }
        
        self.delegate = self
        
        var itens: [UITabBarItem] = self.tabBar.items!
        let catalogItem: UITabBarItem = itens[0]
        let perfilItem: UITabBarItem = itens[1]
        
        catalogItem.image = UIImage(named: "ExploreOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        catalogItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.appLightGrey()], for: UIControlState.normal)
        catalogItem.selectedImage = UIImage(named: "Explore")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        catalogItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.appGrey()], for: UIControlState.selected)
        catalogItem.title = "Explorar"
        
        perfilItem.image = UIImage(named: "ProfileOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        perfilItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.appLightGrey()], for: UIControlState.normal)
        perfilItem.selectedImage = UIImage(named: "Profile")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        perfilItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.appGrey()], for: UIControlState.selected)
        perfilItem.title = "Perfil"
    }
    
    
    
}
