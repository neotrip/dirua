//
//  PerfilController.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 29/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class PerfilController: UIViewController {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var foodTrucksTableView: UITableView!
    @IBOutlet weak var userProfilePictureView: FBSDKProfilePictureView!
    @IBOutlet weak var ratingByMeButton: UIButton!
    @IBOutlet weak var wishListButton: UIButton!
    @IBOutlet weak var fbLoginButton: FBSDKLoginButton!
    @IBOutlet weak var loginView: UIView!
    
    var loggoutGesture: UITapGestureRecognizer?
    
    var perfilTab: PerfilTab = .RATING {
        didSet {
            let labelFontBold = UIFont(name: "HelveticaNeue-Bold", size: 15)
            let labelFont = UIFont(name: "HelveticaNeue", size: 15)
            
            if (perfilTab == .RATING){
//                markView.center = ratingByMeButton.center
                
                ratingByMeButton.titleLabel?.font = labelFontBold
                wishListButton.titleLabel?.font = labelFont
            } else {
//                markView.center = wishListButton.center
                
                ratingByMeButton.titleLabel?.font = labelFont
                wishListButton.titleLabel?.font = labelFontBold
            }
        }
    }
    
    var logged = false {
        didSet {
            if let gesture = loggoutGesture{
                coverImageView.removeGestureRecognizer(gesture)
            }
            
            print("didSet")
            if (logged){
                print("Facebook logged")
                loginView.isHidden = true
                
                wishListButton.isEnabled = true
                ratingByMeButton.isEnabled = true
                
                userProfilePictureView.profileID = ServerManager.sharedInstance.user.facebook
                usernameLabel.text = ServerManager.sharedInstance.user.username
                
                loggoutGesture = UITapGestureRecognizer(target: self, action: #selector(PerfilController.loggout))
                coverImageView.addGestureRecognizer(loggoutGesture!)
            } else {
                print("Facebook not logged")
                loginView.isHidden = false
                
                wishListButton.isEnabled = false
                ratingByMeButton.isEnabled = false
                
                FacebookManager.sharedInstance.logout()
                
                userProfilePictureView.profileID = ""
                usernameLabel.text = ""
                
                tableViewObject = []
                self.foodTrucksTableView.reloadData()
            }
        }
    }
    
    var tableViewObject: [AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fbLoginButton.delegate = self
        
        if (FBSDKAccessToken.current() != nil && ServerManager.sharedInstance.user != nil){
            logged = true
        } else {
            logged = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        userProfilePictureView.circle()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is FoodTruckController{
            let vc  = segue.destination as! FoodTruckController
            vc.truck = sender as! Truck
        }
    }
    
    @IBAction func ratingByMe(_ sender: AnyObject) {
        perfilTab = .RATING
        self.tableViewObject = []
        foodTrucksTableView!.reloadData()
        
//        foodTrucksTableView!.startLoading(style: <#T##UIActivityIndicatorViewStyle#>)
        
        ServerManager.sharedInstance.rating({ (error, ratings) in
            if (error == nil){
                self.tableViewObject = ratings
                DispatchQueue.main.async(execute: {
                    self.foodTrucksTableView!.reloadData()
                })
            }
            
            DispatchQueue.main.async(execute: {
                self.foodTrucksTableView!.stopLoading()
            })
        })
    }
    
    @IBAction func wishList(_ sender: AnyObject) {
        perfilTab = .WISHLIST
        tableViewObject = []
        foodTrucksTableView!.reloadData()
        
//        foodTrucksTableView!.startLoading()
        ServerManager.sharedInstance.wishlist({ (error, trucks) in
            if (error == nil){
                self.tableViewObject = trucks
                DispatchQueue.main.async(execute: {
                    self.foodTrucksTableView!.reloadData()
                })
            }
            
            DispatchQueue.main.async(execute: {
                self.foodTrucksTableView!.stopLoading()
            })
        })
    }
    
    func loggout(){
        loginButtonDidLogOut(fbLoginButton)
    }
}

extension PerfilController: FBSDKLoginButtonDelegate {
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        logged = false
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        ServerManager.sharedInstance.facebooksignup(facebook: FacebookManager.sharedInstance.fbID(), facebookToken: FacebookManager.sharedInstance.token(), completion: { (error, success) in
            DispatchQueue.main.async(execute: {
                self.logged = success
            })
        })
    }
    
}

extension PerfilController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewObject.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if (perfilTab == .WISHLIST){
            let truckCell = tableView.dequeueReusableCell(withIdentifier: FOOD_TRUCK_CELL_IDENTIFIER) as! FoodTruckTableViewCell
            truckCell.fill(tableViewObject[indexPath.row] as! Truck)
            
            cell = truckCell
        } else {
            let ratingCell = tableView.dequeueReusableCell(withIdentifier: RATING_CELL_IDENTIFIER) as! RatingTableViewCell
            ratingCell.fill(tableViewObject[indexPath.row] as! Rating)
            
            cell = ratingCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let truckCell = cell as? FoodTruckTableViewCell {
            truckCell.sessionAvatar?.cancel()
            truckCell.sessionCover?.cancel()
        } else if let ratingCell = cell as? RatingTableViewCell {
            ratingCell.sessionAvatar?.cancel()
            ratingCell.sessionCover?.cancel()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (perfilTab == .WISHLIST){
            let selectedTruck = tableViewObject[indexPath.row] as! Truck
            performSegue(withIdentifier: "showTruck", sender: selectedTruck)
        }
    }
    
}
