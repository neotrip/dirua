//
//  CacheImage.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 01/09/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class CacheManager: NSObject {
    
    static let sharedInstance = CacheManager()
    
    var images: Dictionary<String, NSData!> = [:]
    
    override init(){
        super.init()
    }
    
    func cacheImage(path path: String, imageData: NSData){
        images[path] = imageData
    }
    
    func image(path path: String) -> UIImage?{
        for pathImage in images.keys{
            if (pathImage == path){
                return UIImage(data: images[path]!)!
            }
        }
        
        return nil
    }
}
