//
//  Agent.swift
//  SportsMatch
//
//  Created by Marcos Kobuchi on 4/13/16.
//  Copyright © 2016 Marcos Kobuchi. All rights reserved.
//

import Foundation

public class Agent {
    
    typealias Headers = Dictionary<String, String>
    //typealias Data = AnyObject!
    typealias Json = Dictionary<String, AnyObject>?
    
    typealias Response = (HTTPURLResponse?, Json, Error?) -> Void
    typealias ResponseData = (HTTPURLResponse?, AnyObject?, Error?) -> Void
    public typealias RequestData = Dictionary<String,AnyObject>
    
    /**
     * Members
     */
    
    var request: NSMutableURLRequest
    let queue = OperationQueue()
    
    /**
     * Initialize
     */
    
    init(method: String, url: String, headers: Headers?) {
        self.request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        self.request.httpMethod = method;
        self.set(header: "Accept", value: "application/vnd.foodtruck.v1")
        if (headers != nil) {
            self.request.allHTTPHeaderFields = headers!
        }
    }
    
    /**
     * GET
     */
    
    class func get(url: String) -> Agent {
        return Agent(method: "GET", url: url, headers: nil)
    }
    
    class func get(url: String, headers: Headers) -> Agent {
        return Agent(method: "GET", url: url, headers: headers)
    }
    
    class func get(url: String, done: @escaping Response) -> Agent {
        return Agent.get(url: url).end(done: done)
    }
    
    class func get(url: String, headers: Headers, done: @escaping Response) -> Agent {
        return Agent.get(url: url, headers: headers).end(done: done)
    }
    
    /**
     * POST
     */
    
    class func post(url: String) -> Agent {
        return Agent(method: "POST", url: url, headers: nil)
    }
    
    class func post(url: String, headers: Headers) -> Agent {
        return Agent(method: "POST", url: url, headers: headers)
    }
    
    class func post(url: String, done: @escaping Response) -> Agent {
        return Agent.post(url: url).end(done: done)
    }
    
    class func post(url: String, headers: Headers, data: AnyObject) -> Agent {
        return Agent.post(url: url, headers: headers).send(data: data)
    }
    
    class func post(url: String, data: AnyObject) -> Agent {
        return Agent.post(url: url).send(data: data)
    }
    
    class func post(url: String, data: AnyObject, done: @escaping Response) -> Agent {
        return Agent.post(url: url, data: data).send(data: data).end(done: done)
    }
    
    class func post(url: String, headers: Headers, data: AnyObject, done: @escaping Response) -> Agent {
        return Agent.post(url: url, headers: headers, data: data).send(data: data).end(done: done)
    }
    
    /**
     * PUT
     */
    
    class func put(url: String) -> Agent {
        return Agent(method: "PUT", url: url, headers: nil)
    }
    
    class func put(url: String, headers: Headers) -> Agent {
        return Agent(method: "PUT", url: url, headers: headers)
    }
    
    class func put(url: String, done: @escaping Response) -> Agent {
        return Agent.put(url: url).end(done: done)
    }
    
    class func put(url: String, headers: Headers, data: AnyObject) -> Agent {
        return Agent.put(url: url, headers: headers).send(data: data)
    }
    
    class func put(url: String, data: AnyObject) -> Agent {
        return Agent.put(url: url).send(data: data)
    }
    
    class func put(url: String, data: AnyObject, done: @escaping Response) -> Agent {
        return Agent.put(url: url, data: data).send(data: data).end(done: done)
    }
    
    class func put(url: String, headers: Headers, data: AnyObject, done: @escaping Response) -> Agent {
        return Agent.put(url: url, headers: headers, data: data).send(data: data).end(done: done)
    }
    
    /**
     * DELETE
     */
    
    class func delete(url: String) -> Agent {
        return Agent(method: "DELETE", url: url, headers: nil)
    }
    
    class func delete(url: String, headers: Headers) -> Agent {
        return Agent(method: "DELETE", url: url, headers: headers)
    }
    
    class func delete(url: String, done: @escaping Response) -> Agent {
        return Agent.delete(url: url).end(done: done)
    }
    
    class func delete(url: String, headers: Headers, done: @escaping Response) -> Agent {
        return Agent.delete(url: url, headers: headers).end(done: done)
    }
    
    /**
     * Methods
     */
    
    func send(data: AnyObject) -> Agent {
        // var error: NSError?
        let json: Data?
        do {
            json = try JSONSerialization.data(withJSONObject: data, options: [])
            print(NSString(data: json!, encoding: String.Encoding.utf8.rawValue)!)
        } catch _ as NSError {
            json = nil
        }
        self.set(header: "Content-Type", value: "application/json")
        self.request.httpBody = json as Data?
        return self
    }
    
    func set(header: String, value: String) -> Agent {
        self.request.setValue(value, forHTTPHeaderField: header)
        return self
    }
    
    func end(done: @escaping Response) -> Agent {
        URLSession(configuration: URLSessionConfiguration.default).dataTask(with: self.request as URLRequest) { data, response, error in
            
            let res = response as! HTTPURLResponse!
            if (error != nil) {
                done(res, nil, error)
                return
            }
            
            var json: Dictionary<String, AnyObject>?
            if (data != nil) {
                do {
                    //print(NSString(data: data!, encoding: NSUTF8StringEncoding)!)
                    json = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String, AnyObject>
                } catch {
                    json = nil
                }
            }
            done(res, json, error)
            
            }.resume()
        
        return self
    }
    
    
    /**
     *  Function to upload image & data using POST request
     *
     *  @param image:NSData       image data of type NSData
     *  @param fieldName:String   field name for uploading image
     *  @param data:RequestData?  optional value of type Dictionary<String,AnyObject>
     *
     *  @return self instance to support function chaining
     */
    func data(image:Data) -> Agent {
        if(image.count > 0 && self.request.httpMethod == "POST") {
            let body = NSMutableData()
            
            let fname = "img.jpg"
            let mimetype = "image/jpg"
            
            //define the data post parameter
            let boundary = "Boundary-\(NSUUID().uuidString)"
            
            //define the multipart request type
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(image)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
        }
        return self
    }
    
}
