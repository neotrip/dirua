//
//  FacebookManager.swift
//  NeoTruck
//
//  Created by Carlos Henrique Cayres on 9/6/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class FacebookManager: NSObject {
    
    static let sharedInstance = FacebookManager()
    
    private var _loginManager: FBSDKLoginManager?
    var loginManager: FBSDKLoginManager {
        get {
            if _loginManager == nil {
                _loginManager = FBSDKLoginManager()
            }
            return _loginManager!
        }
        
        
    }
    
    func requestFacebookUserData(_ completion: @escaping (NSDictionary) -> Void){
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,gender,email,name,picture.width(480).height(480)"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil){
                print("Error: \(error.debugDescription)")
            } else {
                let dic = result as! NSDictionary
                completion(dic)
            }
        })
    }
    
    func requestFacebookUserData(id: String, completion: @escaping (NSDictionary) -> Void){
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: id, parameters: ["fields":"id,gender,email,name,picture.width(480).height(480)"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil){
                print("Error: \(error.debugDescription)")
            } else {
                let dic = result as! NSDictionary
                completion(dic)
            }
        })
    }
    
    var cacheName:[String:String] = [:]
    func name(id: String, completion: @escaping (String?) -> ()) {
        if let name = cacheName[id]{
            completion(name)
        } else {
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: id, parameters: ["fields":"name"])
            graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                if ((error) != nil){
                    print("Error: \(error.debugDescription)")
                } else {
                    let dic = result as! NSDictionary
                    if let name = dic["name"] as? String {
                        self.cacheName[id] = name
                        completion(name)
                    }
                }
            })
        }
    }
    
    
    func token() -> String{
        if let token = FBSDKAccessToken.current(){
            return token.tokenString
        }
        
        return ""
    }
    
    func fbID() -> String{
        if let token = FBSDKAccessToken.current(){
            return token.userID
        }
        
        return ""
    }
    
    func logout(){
        loginManager.logOut()
        ServerManager.sharedInstance.user = nil
    }
}
