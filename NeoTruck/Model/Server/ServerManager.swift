//
//  ServerManager.swift
//  neotrip
//
//  Created by Marcos Kobuchi on 5/30/16.
//  Copyright © 2016 neotripteam. All rights reserved.
//

import UIKit
import MapKit

protocol Object : class {
    var id:String? { get set }
    func dictionary() -> NSDictionary
}

class ServerManager: NSObject {

    var user: User!
    var categories: [Category] = []
    var trucks: [Truck] = []
    var wishlists: [Truck] = []
    
    let server = "http://api.dirua.me/"
//    let server = "http://api.foodtruck.dev:3000/"
//    let server = "http://api.localhost.dev:3000/"
//    let server = "http://api.lvh.me:3000/"
    static let sharedInstance = ServerManager()
    
    fileprivate override init() {
        super.init()
    }
    
    /* SESSION */
    func version(_ completion:@escaping (_ error: Error?) -> Void) {
        Agent.get(url: server + "trucks")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                } else {
                    print(response!.statusCode)
                    print(json)
                }
                completion(error)
            })
    }
    
    func signup(email: String, username: String, password: String, completion: ((_ error: Error?, _ success:Bool) -> Void)! = nil) {
        let newUser = ["email": email, "password": password, "username": username]
        Agent.post(url: server + "users")
            .send(data: newUser as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                        case 201:
                            self.user = User(username: username, email: email, aToken: json!["auth_token"] as! String)
                            completion?(nil, true)
                        case 422: print("Not created")
                        completion?(ERROR_422, false)
                        case 500: print("Internal server error")
                        completion?(ERROR_500, false)
                        default:
                        completion?(ERROR_500, false)
                    }
                }
                
                
            })
    }
    
    
    func facebooksignup(facebook: String, facebookToken: String, completion: ((_ error: Error?, _ success:Bool) -> Void)! = nil) {
        let newUser = ["facebook": facebook, "fb_token": facebookToken]
        Agent.post(url: server + "sessions")
            .send(data: newUser as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        FacebookManager.sharedInstance.requestFacebookUserData(id: facebook, completion: { (responseDic) in
                            if (response != nil){
                                self.user = User(dic: responseDic, aToken: json!["auth_token"] as! String)
                                completion?(nil, true)
                            } else {
                                completion?(ERROR_422, false)
                            }
                        })
                    case 422: print("Not created")
                    completion?(ERROR_422, false)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    func login(_ login: String, password: String, completion: ((_ error: Error?, _ success:Bool) -> Void)! = nil) {
        let login = ["login": login, "password": password]
        Agent.post(url: server + "sessions")
            .send(data: login as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 201:
                        self.user = User(username: json!["username"]! as! String, email: json!["email"] as! String, aToken: json!["auth_token"] as! String)
                        completion?(nil, true)
                    case 400: print("Username/Email incorrect")
                        completion?(ERROR_400, false)
                    case 500: print("Internal server error")
                        completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    
/*****/
    func logout(_ completion: ((_ error: Error?, _ success:Bool) -> Void)! = nil) {
        Agent.delete(url: server + "sessions")
            .set(header: "Authorization", value: user.authToken)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 204:print("Logouted")
                        completion?(error!, false)
                    case 401: print("Token incorrect")
                        completion?(ERROR_401, false)
                    case 500: print("Internal server error")
                        completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    /* TRUCK */
    func trucks(_ completion: ((_ error: Error?, _ trucks: [Truck]) -> Void)! = nil) {
        Agent.get(url: server + "trucks")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var trucks: [Truck] = []
                        let trucksDict = json!["trucks"] as! [NSDictionary]
                        for truckDict in trucksDict{
                            var categories: [Category] = []
                            if let categoriesDict = truckDict["categories"]{
                                if (categoriesDict != nil){
                                    for categoryDict in categoriesDict as! NSArray{
                                        categories.append(Category(id: categoryDict as! String, name: nil, image: nil))
                                    }
                                }
                            }
                            
                            trucks.append(Truck(id: truckDict["id"] as! String, name: truckDict["name"] as! String, avatar: truckDict["avatar"] as? String, cover: truckDict["cover"] as? String, descInfo: "", phoneNumber: "", categories: categories, dishes: [], schedules: [], rate: truckDict["rate"] as? Double, raters: truckDict["evaluators"] as? Int))
                        }
                        
                        completion?(nil, trucks)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    func truck(basicTruck: Truck, completion: ((_ error: Error?, _ truck: Truck?) -> Void)! = nil) {
        Agent.get(url: server + "trucks?id=\(basicTruck.id!)")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, nil)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var dishes: [Dish] = []
                        if let dishesDict = json!["dishes"] as? [NSDictionary] {
                            for dishDict in dishesDict{
                                dishes.append(Dish(id: dishDict["id"] as! String, name: dishDict["name"] as! String, price: (dishDict["price"] as? NSNumber)!, info: dishDict["description"] as! String, image: dishDict["picture"] as? String))
                            }
                        }
                        
                        var schedules: [Schedule] = []
                        if let schedulesDict = json!["schedules"] as? [NSDictionary] {
                            for scheduleDict in schedulesDict{
                                schedules.append(Schedule(latitude: (scheduleDict["latitude"] as? CLLocationDegrees)!, longitude: (scheduleDict["longitude"] as? CLLocationDegrees)!, from: (scheduleDict["from"] as? String)!, to: (scheduleDict["to"] as? String)!))
                            }
                        }
                        
                        basicTruck.name = json!["name"] as! String
                        basicTruck.avatar = json!["avatar"] as? String
                        basicTruck.cover = json!["cover"] as? String
                        basicTruck.descInfo = json!["description"] as? String
                        basicTruck.phoneNumber = json!["phonenumber"] as? String
                        basicTruck.schedules = schedules
                        basicTruck.dishes = dishes
                        basicTruck.rate = json!["rate"] as? Double
                        basicTruck.raters = json!["evaluators"] as? Int
                        
                        completion?(nil, basicTruck)
                    case 404: print("Not found")
                    completion?(ERROR_404, nil)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, nil)
                    default:
                        completion?(ERROR_500, nil)
                    }
                }
            })
    }
    
    func trucks(search: String, completion: ((_ error: Error?, _ trucks: [Truck]) -> Void)! = nil) {
        Agent.get(url: server + "trucks?name=\(search)")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var trucks: [Truck] = []
                        let trucksDict = json!["trucks"] as! [NSDictionary]
                        for truckDict in trucksDict{
                            var categories: [Category] = []
                            if let categoriesDict = truckDict["categories"] {
                                for categoryDict in categoriesDict as! NSArray{
                                    categories.append(Category(id: categoryDict as! String, name: nil, image: nil))
                                }
                            }
                            
                            trucks.append(Truck(id: truckDict["id"] as! String, name: truckDict["name"] as! String, avatar: truckDict["avatar"] as? String, cover: truckDict["cover"] as? String, descInfo: "", phoneNumber: "", categories: categories, dishes: [], schedules: [], rate: truckDict["rate"] as? Double, raters: truckDict["evaluators"] as? Int))
                        }
                        
                        completion?(nil, trucks)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    func trucks(idCategory: String, completion: ((_ error: Error?, _ trucks: [Truck]) -> Void)! = nil) {
        Agent.get(url: server + "categories?id=\(idCategory)")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var trucks: [Truck] = []
                        let trucksDict = json!["trucks"] as! [NSDictionary]
                        for truckDict in trucksDict{
                            var categories: [Category] = []
                            if let categoriesDict = truckDict["categories"] {
                                for categoryDict in categoriesDict as! NSArray{
                                    categories.append(Category(id: categoryDict as! String, name: nil, image: nil))
                                }
                            }
                            
                            trucks.append(Truck(id: truckDict["id"] as! String, name: truckDict["name"] as! String, avatar: truckDict["avatar"] as? String, cover: truckDict["cover"] as? String, descInfo: "", phoneNumber: "", categories: categories, dishes: [], schedules: [], rate: truckDict["rate"] as? Double, raters: truckDict["evaluators"] as? Int))
                        }
                        
                        completion?(nil, trucks)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    func trucks(time: Date?, completion: ((_ error: Error?, _ trucks: [Truck]) -> Void)! = nil) {
        var request = "schedules"
        if (time != nil){
            request += "?time=\(time!.string())"
        }
        Agent.get(url: server + request)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var trucks: [Truck] = []
                        let trucksDict = json!["trucks"] as! [NSDictionary]
                        for truckDict in trucksDict{
                            var categories: [Category] = []
                            if let categoriesDict = truckDict["categories"] {
                                for categoryDict in categoriesDict as! NSArray{
                                    categories.append(Category(id: categoryDict as! String, name: nil, image: nil))
                                }
                            }
                            
                            var schedules: [Schedule] = []
                            if let schedulesDict = truckDict["schedules"] as? [NSDictionary] {
                                for scheduleDict in schedulesDict{
                                    schedules.append(Schedule(latitude: (scheduleDict["latitude"] as? CLLocationDegrees)!, longitude: (scheduleDict["longitude"] as? CLLocationDegrees)!, from: (scheduleDict["from"] as? String)!, to: (scheduleDict["to"] as? String)!))
                                }
                            }
                            
                            trucks.append(Truck(id: truckDict["id"] as! String, name: truckDict["name"] as! String, avatar: truckDict["avatar"] as? String, cover: truckDict["cover"] as? String, descInfo: "", phoneNumber: "", categories: categories, dishes: [], schedules: schedules, rate: truckDict["rate"] as? Double, raters: truckDict["evaluators"] as? Int))
                        }
                        
                        completion?(nil, trucks)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    func trucks(to: Date, from: Date, completion: ((_ error: Error?, _ trucks: [Truck]) -> Void)! = nil) {
        Agent.get(url: server + "schedule?from=\(from.string())&to=\(to.string())")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var trucks: [Truck] = []
                        let trucksDict = json!["trucks"] as! [NSDictionary]
                        for truckDict in trucksDict{
                            var categories: [Category] = []
                            if let categoriesDict = truckDict["categories"] as? [NSDictionary] {
                                for categoryDict in categoriesDict{
                                    categories.append(Category(id: categoryDict as! String, name: nil, image: nil))
                                }
                            }
                            
                            var schedules: [Schedule] = []
                            if let schedulesDict = truckDict["schedules"] as? [NSDictionary] {
                                for scheduleDict in schedulesDict{
                                    schedules.append(Schedule(latitude: (scheduleDict["latitude"] as? CLLocationDegrees)!, longitude: (scheduleDict["longitude"] as? CLLocationDegrees)!, from: (scheduleDict["from"] as? String)!, to: (scheduleDict["to"] as? String)!))
                                }
                            }
                            
                            trucks.append(Truck(id: truckDict["id"] as! String, name: truckDict["name"] as! String, avatar: truckDict["avatar"] as? String, cover: truckDict["cover"] as? String, descInfo: "", phoneNumber: "", categories: categories, dishes: [], schedules: schedules, rate: truckDict["rate"] as? Double, raters: truckDict["evaluators"] as? Int))
                        }
                        
                        completion?(nil, trucks)
                    case 422: print("Missing Parameter")
                    completion?(ERROR_422, [])
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    
    
//    func truck(id id: String, name: String, info: String, completion: ((error: Error!, updatedTruck: Truck!) -> Void)! = nil) {
//        let updatedTruck = ["id": id, "name": name, "description": info]
//        Agent.put(url: server + "trucks")
//            .set(header: "Authorization", value: user.authToken)
//            .send(updatedTruck)
//            .end({(response: NSHTTPURLResponse!, json: Agent.Json, error: Error!) -> Void in
//                if (error != nil) {
//                    print("erro: \(error)")
//                    completion?(error: error!, updatedTruck: nil)
//                } else {
//                    let statusCode = response!.statusCode
//                    switch statusCode {
//                    case 204:
//                        completion?(error: nil, updatedTruck: Truck(id: id, name: name, info: info))
//                    case 403: print("Forbidden")
//                    completion?(error: ERROR_403, updatedTruck: nil)
//                    case 422: print("Missing Parameter")
//                    completion?(error: ERROR_422, updatedTruck: nil)
//                    case 500: print("Internal server error")
//                    completion?(error: ERROR_500, updatedTruck: nil)
//                    default:
//                        completion?(error: ERROR_500, updatedTruck: nil)
//                    }
//                }
//            })
//    }
    
    /* DISH */
    func dish(_ name: String, price: NSNumber, info: String, idTruck: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let newDish = ["name": name, "price": price, "description": info, "id": idTruck] as [String : Any]
        Agent.post(url: server + "dishes")
            .set(header: "Authorization", value: user.authToken)
            .send(data: newDish as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 201:
                        completion?(error!, false)
                    case 403: print("Forbidden")
                    completion?(ERROR_403, false)
                    case 422: print("Missing Parameter")
                    completion?(ERROR_422, false)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
                
                
            })
    }
    
    func dish(id: String, name: String, price: NSNumber, info: String, completion: ((_ error: Error?, _ updatedDish: Dish?) -> Void)! = nil) {
        let updatedDish = ["id": id, "name": name, "description": info, "price": price] as [String : Any]
        Agent.put(url: server + "dishes")
            .set(header: "Authorization", value: user.authToken)
            .send(data: updatedDish as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, nil)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 204:
                        completion?(nil, Dish(id: id, name: name, price: price, info: info))
                    case 403: print("Forbidden")
                    completion?(ERROR_403, nil)
                    case 422: print("Missing Parameter")
                    completion?(ERROR_422, nil)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, nil)
                    default:
                        completion?(ERROR_500, nil)
                    }
                }
            })
    }
    
    func dish(id idDish: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let deletedDish = ["id": idDish]
        Agent.delete(url: server + "dishes")
            .set(header: "Authorization", value: user.authToken)
            .send(data: deletedDish as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    print(statusCode)
                    switch statusCode {
                    case 204:print("Deleted")
                    completion?(error!, false)
                    case 403: print("Forbidden")
                    completion?(ERROR_403, false)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    /* SCHEDULE */
    func schedule(latitude: CLLocationDegrees, longitude: CLLocationDegrees, from: Date, to: Date, idTruck: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let newSchedule = ["latitude": latitude, "longitude": longitude, "from": from.string(), "to": to.string(), "id": idTruck] as [String : Any]
        Agent.post(url: server + "schedules")
            .set(header: "Authorization", value: user.authToken)
            .send(data: newSchedule as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 201:
                        completion?(error!, false)
                    case 403: print("Forbidden")
                    completion?(ERROR_403, false)
                    case 422: print("Missing Parameter")
                    completion?(ERROR_422, false)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    func schedule(id: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees, from: Date, to: Date, completion: ((_ error: Error?, _ updatedSchedule: Schedule?) -> Void)! = nil) {
        let updatedSchedule = ["id": id, "latitude": latitude, "longitude": longitude, "from": from.string(), "to": to.string()] as [String : Any]
        Agent.put(url: server + "schedules")
            .set(header: "Authorization", value: user.authToken)
            .send(data: updatedSchedule as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, nil)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 204:
                        completion?(nil, Schedule(id: id, latitude: latitude, longitude: longitude, from: from.string(), to: to.string()))
                    case 403: print("Forbidden")
                    completion?(ERROR_403, nil)
                    case 422: print("Missing Parameter")
                    completion?(ERROR_422, nil)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, nil)
                    default:
                        completion?(ERROR_500, nil)
                    }
                }
            })
    }
    
    func schedule(idSchedule: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let deletedSchedule = ["id": idSchedule]
        Agent.delete(url: server + "schedules")
            .send(data: deletedSchedule as AnyObject)
            .set(header: "Authorization", value: user.authToken)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(error!, false)
                } else {
                    let statusCode = response!.statusCode
                    print(statusCode)
                    switch statusCode {
                    case 204:print("Deleted")
                    completion?(error!, false)
                    case 403: print("Forbidden")
                    completion?(ERROR_403, false)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    /* CATEGORY */
    func categories(_ completion: ((_ error: Error?, _ categories: [Category]) -> Void)! = nil) {
        Agent.get(url: server + "categories")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var categories: [Category] = []
                        let categoriesDict = json!["categories"] as! [NSDictionary]
                        for categoryDict in categoriesDict{
                            categories.append(Category(id: categoryDict["id"] as! String, name: categoryDict["name"] as? String, image: categoryDict["image"] as? String))
                        }
                        
                        completion?(nil, categories)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    /* IMAGE */
    var defaultImage:UIImage?
    var cache:[String:UIImage] = [:]
    
    func image(_ url:String?, completion: @escaping (_ error: Error?, _ image: UIImage?)->()) -> (UIImage?, URLSessionDataTask?) {
        var img: UIImage? = defaultImage
        var session: URLSessionDataTask?
        
        if let imageURL = url {
            if let image = cache[imageURL] {
                img = image
                completion(nil, img)
            } else {
                let imagePath = server + imageURL
                let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
                session = defaultSession.dataTask(with: URL(string: imagePath)!, completionHandler: { (data, response, error) in
                    if error != nil {
                        completion(error as Error?, nil)
                    } else {
                        if let imageData = data {
                            let image = UIImage(data: imageData)
                            self.cache[imageURL] = image
                            img = image
                            completion(nil, img)
                        } else {
                            //let err = Error(domain: "server", code: 0, userInfo: nil)
                            completion(error, nil)
                        }
                    }
                })
                
                session!.resume()
            }
            
        } else {
            completion(nil, nil)
        }
        
        return (nil, session)
    }
    
    /* PERFIL */
    func wishlist(_ completion: ((_ error: Error?, _ trucks: [Truck]) -> Void)! = nil) {
        Agent.get(url: server + "wishlists")
            .set(header: "Authorization", value: user.authToken)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var trucks: [Truck] = []
                        let trucksDict = json!["trucks"] as! [NSDictionary]
                        for truckDict in trucksDict{
                            var categories: [Category] = []
                            if let categoriesDict = truckDict["categories"] {
                                if (categoriesDict != nil){
                                    for categoryDict in categoriesDict as! NSArray{
                                        categories.append(Category(id: categoryDict as! String, name: nil, image: nil))
                                    }
                                }
                            }
                            
                            trucks.append(Truck(id: truckDict["id"] as! String, name: truckDict["name"] as! String, avatar: truckDict["avatar"] as? String, cover: truckDict["cover"] as? String, descInfo: "", phoneNumber: "", categories: categories, dishes: [], schedules: [], rate: truckDict["rate"] as? Double, raters: truckDict["evaluators"] as? Int))
                        }
                        
                        ServerManager.sharedInstance.wishlists = trucks
                        completion?(nil, trucks)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    func wishlist(idAddTruck: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let addWishlist = ["id": idAddTruck]
        Agent.post(url: server + "wishlists")
            .set(header: "Authorization", value: user.authToken)
            .send(data: addWishlist as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 201:
                        completion?(nil, true)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    func wishlist(idDeleteTruck: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let deleteWishlist = ["id": idDeleteTruck]
        Agent.delete(url: server + "wishlists")
            .set(header: "Authorization", value: user.authToken)
            .send(data: deleteWishlist as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 204:
                        completion?(nil, true)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    func rating(_ completion: ((_ error: Error?, _ ratings: [Rating]) -> Void)! = nil) {
        Agent.get(url: server + "ratings")
            .set(header: "Authorization", value: user.authToken)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var ratings: [Rating] = []
                        let ratingsDict = json!["ratings"] as! [NSDictionary]
                        for ratingDict in ratingsDict{
                            let truckDict = ratingDict["truck"] as! NSDictionary
                            
                            var categories: [Category] = []
                            if let categoriesDict = truckDict["categories"] {
                                for categoryDict in categoriesDict as! NSArray{
                                    categories.append(Category(id: categoryDict as! String, name: nil, image: nil))
                                }
                            }
                            
                            ratings.append(Rating(id: ratingDict["id"] as! String, general: ratingDict["general"] as! NSNumber, comment: ratingDict["comment"] as? String, truck: Truck(id: truckDict["id"] as! String, name: truckDict["name"] as! String, avatar: truckDict["avatar"] as? String, cover: truckDict["cover"] as? String, descInfo: "", phoneNumber: "", categories: categories, dishes: [], schedules: [], rate: truckDict["rate"] as? Double, raters: truckDict["evaluators"] as? Int)))
                        }
                        
                        completion?(nil, ratings)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }
    
    func ratings(idTruck: String, completion: ((_ error: Error?, _ ratings: [Rating]) -> Void)! = nil) {
        Agent.get(url: server + "ratings?id=\(idTruck)")
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, [])
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 200:
                        var ratings: [Rating] = []
                        let ratingsDict = json!["ratings"] as! [NSDictionary]
                        for ratingDict in ratingsDict{
                            ratings.append(Rating(id: ratingDict["id"] as! String, facebook: ratingDict["facebook"] as? String, general: ratingDict["general"] as! NSNumber, comment: ratingDict["comment"] as? String, createdAt: ratingDict["created_at"] as? String))
                        }
                        
                        completion?(nil, ratings)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, [])
                    default:
                        completion?(ERROR_500, [])
                    }
                }
            })
    }

    
    func rating(idDeleteRating: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let deleteRating = ["id": idDeleteRating]
        Agent.delete(url: server + "ratings")
            .set(header: "Authorization", value: user.authToken)
            .send(data: deleteRating as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 204:
                        completion?(nil, false)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }
    
    func rating(idAddTruck: String, general: NSNumber, comment: String, completion: ((_ error: Error?, _ success: Bool) -> Void)! = nil) {
        let addRating = ["id": idAddTruck, "general": general.intValue, "comment": comment, "created_at": Date().string()] as [String : Any]
        Agent.post(url: server + "ratings")
            .set(header: "Authorization", value: user.authToken)
            .send(data: addRating as AnyObject)
            .end(done: {(response: HTTPURLResponse?, json: Agent.Json, error: Error?) -> Void in
                if (error != nil) {
                    print("erro: \(error)")
                    completion?(nil, false)
                } else {
                    let statusCode = response!.statusCode
                    switch statusCode {
                    case 201:
                        completion?(nil, false)
                    case 500: print("Internal server error")
                    completion?(ERROR_500, false)
                    default:
                        completion?(ERROR_500, false)
                    }
                }
            })
    }

    
}
