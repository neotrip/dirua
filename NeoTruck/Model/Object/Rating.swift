//
//  Rating.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 14/09/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class Rating: NSObject {

    var id: String!
    var facebook: String?
    var general: NSNumber!
    var comment: String?
    var createdAt: Date?
    var truck: Truck!
    
    convenience init(id: String, general: NSNumber, comment: String?, truck: Truck){
        self.init()
        self.id = id
        self.general = general
        self.comment = comment
        self.truck = truck
    }
    
    convenience init(id: String, facebook: String?, general: NSNumber, comment: String?, createdAt: String?){
        self.init()
        self.id = id
        self.facebook = facebook
        self.general = general
        self.comment = comment
        if let date = createdAt{
            self.createdAt = Date.date(date)
        }
    }
    
    func fillTruck(){
        for alltruck in ServerManager.sharedInstance.trucks{
            if (truck.id == alltruck.id){
                truck.name = alltruck.name
                truck.avatar = alltruck.avatar
                truck.cover = alltruck.cover
                truck.categories = alltruck.categories
                truck.rate = alltruck.rate
                truck.raters = alltruck.raters
                break
            }
        }
    }
    
}
