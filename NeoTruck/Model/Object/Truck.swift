//
//  Truck.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 10/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class Truck: NSObject {

    var id: String!
    var name: String!
    var avatar: String?
    var cover: String?
    var descInfo: String?
    var phoneNumber: String?
    var dishes: [Dish]!
    var schedules: [Schedule]!
    var categories: [Category]!
    
    var logoImage:UIImage?
    var coverImage:UIImage?
    
    var rate:Double?
    var raters:Int?
    
    
    init(id: String, name: String, avatar: String?, cover: String?, descInfo: String, phoneNumber: String?, categories: [Category], dishes: [Dish], schedules: [Schedule], rate:Double?, raters:Int?) {
        self.id = id
        self.name = name
        self.avatar = avatar
        self.cover = cover
        self.descInfo = descInfo
        self.categories = categories
        self.dishes = dishes
        self.schedules = schedules
        self.phoneNumber = phoneNumber
        
        self.logoImage = nil
        self.coverImage = nil
        
        self.rate = rate
        self.raters = raters
    }
    
    class func sort(){
        ServerManager.sharedInstance.trucks.sort {
            if ($0.isOpen() && !$1.isOpen()){ return true }
            if (!$0.isOpen() && $1.isOpen()){ return false }
            
            if ($0.rate != nil && $1.rate != nil){
                if ($0.rate! > $1.rate!) { return true }
                else if ($0.rate! < $1.rate!) { return false }
                return $0.name! < $1.name!
            }
            
            if ($0.rate == nil && $1.rate != nil){ return false }
            if ($0.rate != nil && $1.rate == nil){ return true }
    
            return $0.name! < $1.name!
        }
    }
    
    func isOpen() -> Bool{
        let now = Date()
        for schedule in self.schedules{
            if (now < schedule.to! && now > schedule.from!){
                return true
            }
        }
        
        return false
    }
    
    class func filter(forCategory: Category) -> [Truck]{
        var result: [Truck] = []
        for truck in ServerManager.sharedInstance.trucks {
            for category in truck.categories {
                if category.id == forCategory.id {
                    result += [truck]
                    break
                }
            }
        }
        
        return result
    }
    
    func fillCategory(){
        for category in self.categories{
            for allcategory in ServerManager.sharedInstance.categories{
                if (category.id == allcategory.id){
                    category.name = allcategory.name
                    category.image = allcategory.image
                    break
                }
            }
        }
    }
}
