//
//  User.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 10/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class User: NSObject {

    var username: String!
    var email: String!
    var authToken: String!
    var facebook: String?
    
    convenience init(username: String, email: String, aToken: String) {
        self.init()
        self.username = username
        self.email = email
        self.authToken = aToken
    }
    
    convenience init(dic: NSDictionary, aToken: String) {
        self.init()
        parserUser(dic)
        self.authToken = aToken
    }
    
    func parserUser(_ dic:NSDictionary){
        self.facebook = dic.value(forKey: "id") as? String
        self.username = dic.value(forKey: "name") as! String
    }

    
    
}
