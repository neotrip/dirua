//
//  Dish.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 10/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class Dish: NSObject {

    var id: String!
    var name: String!
    var price: NSNumber?
    var info: String?
    var image: String?
    
    init(id: String, name: String, price: NSNumber, info: String) {
        self.id = id
        self.name = name
        self.info = info
        self.price = price
    }
    
    convenience init(id: String, name: String, price: NSNumber, info: String, image: String?) {
        self.init(id: id, name: name, price: price, info: info)
        self.image = image
    }
    
}
