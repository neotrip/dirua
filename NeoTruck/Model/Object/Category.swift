//
//  Category.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 30/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit

class Category: NSObject {
    
    var id: String!
    var name: String?
    var image: String?
    
    init(id: String, name: String?, image: String?) {
        self.id = id
        self.name = name
        self.image = image
    }
    
    func string() -> String{
        return "id: \(id), name: \(name)"
    }
    
    class func all() -> Category{
        return Category(id: "all", name: "TODOS", image: nil)
    }
}
