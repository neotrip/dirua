//
//  Schedule.swift
//  NeoTruck
//
//  Created by Joao Pedro Fabiano Franco on 11/08/16.
//  Copyright © 2016 neotruck. All rights reserved.
//

import UIKit
import MapKit

class Schedule: NSObject {
    
    var id: String?
    var coordinate: CLLocationCoordinate2D?
    var from: Date?
    var to: Date?
    
    init(id: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees, from: String, to: String) {
        self.id = id
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.from = Date.date(from)
        self.to = Date.date(to)
    }
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees, from: String, to: String) {
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.from = Date.date(from)
        self.to = Date.date(to)
    }
    
    func string() -> String{
         return "id: \(id), coordinate: \(coordinate!), from: \(from!), to: \(to!)"
    }

}
